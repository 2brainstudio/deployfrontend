webpackJsonp([1],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/app/AdminModule/admin.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__directorio_components_list_directorio_component__ = __webpack_require__("../../../../../src/app/AdminModule/directorio/components/list.directorio.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__directorio_components_edit_directorio_component__ = __webpack_require__("../../../../../src/app/AdminModule/directorio/components/edit.directorio.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directorio_components_new_directorio_component__ = __webpack_require__("../../../../../src/app/AdminModule/directorio/components/new.directorio.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__roles_components_list_rol_component__ = __webpack_require__("../../../../../src/app/AdminModule/roles/components/list.rol.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__roles_components_edit_rol_component__ = __webpack_require__("../../../../../src/app/AdminModule/roles/components/edit.rol.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__roles_components_new_rol_component__ = __webpack_require__("../../../../../src/app/AdminModule/roles/components/new.rol.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__usuarios_components_list_usuario_component__ = __webpack_require__("../../../../../src/app/AdminModule/usuarios/components/list.usuario.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__usuarios_components_edit_usuario_component__ = __webpack_require__("../../../../../src/app/AdminModule/usuarios/components/edit.usuario.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__usuarios_components_new_usuario_component__ = __webpack_require__("../../../../../src/app/AdminModule/usuarios/components/new.usuario.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__admin_routing_module__ = __webpack_require__("../../../../../src/app/AdminModule/admin.routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_directorio_services__ = __webpack_require__("../../../../../src/app/AdminModule/services/directorio.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_roles_services__ = __webpack_require__("../../../../../src/app/AdminModule/services/roles.services.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var AdminModule = (function () {
    function AdminModule() {
    }
    return AdminModule;
}());
AdminModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["k" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_12__admin_routing_module__["a" /* AdminRoutingModule */]
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__directorio_components_list_directorio_component__["a" /* ListDirectorioComponent */],
            __WEBPACK_IMPORTED_MODULE_4__directorio_components_edit_directorio_component__["a" /* EditDirectorioComponent */],
            __WEBPACK_IMPORTED_MODULE_5__directorio_components_new_directorio_component__["a" /* NewDirectorioComponent */],
            __WEBPACK_IMPORTED_MODULE_6__roles_components_list_rol_component__["a" /* ListRolComponent */],
            __WEBPACK_IMPORTED_MODULE_7__roles_components_edit_rol_component__["a" /* EditRolComponent */],
            __WEBPACK_IMPORTED_MODULE_8__roles_components_new_rol_component__["a" /* NewRolComponent */],
            __WEBPACK_IMPORTED_MODULE_9__usuarios_components_list_usuario_component__["a" /* ListUsuarioComponent */],
            __WEBPACK_IMPORTED_MODULE_10__usuarios_components_edit_usuario_component__["a" /* EditUsuarioComponent */],
            __WEBPACK_IMPORTED_MODULE_11__usuarios_components_new_usuario_component__["a" /* NewUsuarioComponent */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_13__services_directorio_services__["a" /* DirectorioServices */],
            __WEBPACK_IMPORTED_MODULE_14__services_roles_services__["a" /* RolesServices */]
        ]
    })
], AdminModule);

//# sourceMappingURL=admin.module.js.map

/***/ }),

/***/ "../../../../../src/app/AdminModule/admin.routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__usuarios_components_list_usuario_component__ = __webpack_require__("../../../../../src/app/AdminModule/usuarios/components/list.usuario.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__usuarios_components_new_usuario_component__ = __webpack_require__("../../../../../src/app/AdminModule/usuarios/components/new.usuario.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__usuarios_components_edit_usuario_component__ = __webpack_require__("../../../../../src/app/AdminModule/usuarios/components/edit.usuario.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var adminRoutes = [
    { path: "admin/usuarios", component: __WEBPACK_IMPORTED_MODULE_2__usuarios_components_list_usuario_component__["a" /* ListUsuarioComponent */] },
    { path: "admin/usuarios/new", component: __WEBPACK_IMPORTED_MODULE_3__usuarios_components_new_usuario_component__["a" /* NewUsuarioComponent */] },
    { path: "admin/usuarios/edit", component: __WEBPACK_IMPORTED_MODULE_4__usuarios_components_edit_usuario_component__["a" /* EditUsuarioComponent */] }
];
var AdminRoutingModule = (function () {
    function AdminRoutingModule() {
    }
    return AdminRoutingModule;
}());
AdminRoutingModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */].forChild(adminRoutes)
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */]
        ]
    })
], AdminRoutingModule);

//# sourceMappingURL=admin.routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/AdminModule/directorio/components/edit.directorio.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditDirectorioComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var EditDirectorioComponent = (function () {
    function EditDirectorioComponent() {
    }
    return EditDirectorioComponent;
}());
EditDirectorioComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'direct-edit',
        template: __webpack_require__("../../../../../src/app/AdminModule/directorio/templates/edit.directorio.component.html")
    })
], EditDirectorioComponent);

//# sourceMappingURL=edit.directorio.component.js.map

/***/ }),

/***/ "../../../../../src/app/AdminModule/directorio/components/list.directorio.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListDirectorioComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ListDirectorioComponent = (function () {
    function ListDirectorioComponent() {
    }
    return ListDirectorioComponent;
}());
ListDirectorioComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'direct-list',
        template: __webpack_require__("../../../../../src/app/AdminModule/directorio/templates/list.directorio.component.html")
    })
], ListDirectorioComponent);

//# sourceMappingURL=list.directorio.component.js.map

/***/ }),

/***/ "../../../../../src/app/AdminModule/directorio/components/new.directorio.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewDirectorioComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var NewDirectorioComponent = (function () {
    function NewDirectorioComponent() {
    }
    return NewDirectorioComponent;
}());
NewDirectorioComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'direct-new',
        template: __webpack_require__("../../../../../src/app/AdminModule/directorio/templates/new.directorio.component.html")
    })
], NewDirectorioComponent);

//# sourceMappingURL=new.directorio.component.js.map

/***/ }),

/***/ "../../../../../src/app/AdminModule/directorio/templates/edit.directorio.component.html":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "../../../../../src/app/AdminModule/directorio/templates/list.directorio.component.html":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "../../../../../src/app/AdminModule/directorio/templates/new.directorio.component.html":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "../../../../../src/app/AdminModule/roles/components/edit.rol.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditRolComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var EditRolComponent = (function () {
    function EditRolComponent() {
    }
    return EditRolComponent;
}());
EditRolComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'rol-edit',
        template: __webpack_require__("../../../../../src/app/AdminModule/roles/templates/edit.rol.component.html")
    })
], EditRolComponent);

//# sourceMappingURL=edit.rol.component.js.map

/***/ }),

/***/ "../../../../../src/app/AdminModule/roles/components/list.rol.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListRolComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ListRolComponent = (function () {
    function ListRolComponent() {
    }
    return ListRolComponent;
}());
ListRolComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'rol-list',
        template: __webpack_require__("../../../../../src/app/AdminModule/roles/templates/list.rol.component.html")
    })
], ListRolComponent);

//# sourceMappingURL=list.rol.component.js.map

/***/ }),

/***/ "../../../../../src/app/AdminModule/roles/components/new.rol.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewRolComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var NewRolComponent = (function () {
    function NewRolComponent() {
    }
    return NewRolComponent;
}());
NewRolComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'rol-new',
        template: __webpack_require__("../../../../../src/app/AdminModule/roles/templates/new.rol.component.html")
    })
], NewRolComponent);

//# sourceMappingURL=new.rol.component.js.map

/***/ }),

/***/ "../../../../../src/app/AdminModule/roles/templates/edit.rol.component.html":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "../../../../../src/app/AdminModule/roles/templates/list.rol.component.html":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "../../../../../src/app/AdminModule/roles/templates/new.rol.component.html":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "../../../../../src/app/AdminModule/services/directorio.services.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__GeneralModule_services_globals__ = __webpack_require__("../../../../../src/app/GeneralModule/services/globals.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__ = __webpack_require__("../../../../angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_jwt__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DirectorioServices; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DirectorioServices = (function () {
    function DirectorioServices(GS, authHttp) {
        this.GS = GS;
        this.authHttp = authHttp;
    }
    DirectorioServices.prototype.getAllDirectorio = function () {
        return this.authHttp.get(this.GS.urlServer + "/directorio/all").map(function (res) { return res.json(); });
    };
    return DirectorioServices;
}());
DirectorioServices = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__GeneralModule_services_globals__["a" /* GlobalServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__GeneralModule_services_globals__["a" /* GlobalServices */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__["AuthHttp"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__["AuthHttp"]) === "function" && _b || Object])
], DirectorioServices);

var _a, _b;
//# sourceMappingURL=directorio.services.js.map

/***/ }),

/***/ "../../../../../src/app/AdminModule/services/roles.services.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular2_jwt__ = __webpack_require__("../../../../angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angular2_jwt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__GeneralModule_services_globals__ = __webpack_require__("../../../../../src/app/GeneralModule/services/globals.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RolesServices; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RolesServices = (function () {
    function RolesServices(authHttp, GS) {
        this.authHttp = authHttp;
        this.GS = GS;
    }
    RolesServices.prototype.getAllRoles = function () {
        return this.authHttp.get(this.GS.urlServer + "/role/all").map(function (res) { return res.json(); });
    };
    return RolesServices;
}());
RolesServices = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_angular2_jwt__["AuthHttp"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_angular2_jwt__["AuthHttp"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__GeneralModule_services_globals__["a" /* GlobalServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__GeneralModule_services_globals__["a" /* GlobalServices */]) === "function" && _b || Object])
], RolesServices);

var _a, _b;
//# sourceMappingURL=roles.services.js.map

/***/ }),

/***/ "../../../../../src/app/AdminModule/usuarios/components/edit.usuario.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditUsuarioComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var EditUsuarioComponent = (function () {
    function EditUsuarioComponent() {
    }
    return EditUsuarioComponent;
}());
EditUsuarioComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'user-edit',
        template: __webpack_require__("../../../../../src/app/AdminModule/usuarios/templates/edit.usuario.component.html")
    })
], EditUsuarioComponent);

//# sourceMappingURL=edit.usuario.component.js.map

/***/ }),

/***/ "../../../../../src/app/AdminModule/usuarios/components/list.usuario.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__UsuarioModule_services_usuario_services__ = __webpack_require__("../../../../../src/app/UsuarioModule/services/usuario.services.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListUsuarioComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListUsuarioComponent = (function () {
    function ListUsuarioComponent(us) {
        this.us = us;
        this.usuarios = [];
    }
    ListUsuarioComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.us.getAllUser().subscribe(function (res) {
            console.log(res);
            _this.usuarios = res.datos;
        }, function (error) {
        });
    };
    return ListUsuarioComponent;
}());
ListUsuarioComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'user-list',
        template: __webpack_require__("../../../../../src/app/AdminModule/usuarios/templates/list.usuario.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__UsuarioModule_services_usuario_services__["a" /* UsuarioServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__UsuarioModule_services_usuario_services__["a" /* UsuarioServices */]) === "function" && _a || Object])
], ListUsuarioComponent);

var _a;
//# sourceMappingURL=list.usuario.component.js.map

/***/ }),

/***/ "../../../../../src/app/AdminModule/usuarios/components/new.usuario.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__UsuarioModule_services_usuario_services__ = __webpack_require__("../../../../../src/app/UsuarioModule/services/usuario.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__UsuarioModule_model_usuario_model__ = __webpack_require__("../../../../../src/app/UsuarioModule/model/usuario.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_directorio_services__ = __webpack_require__("../../../../../src/app/AdminModule/services/directorio.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_roles_services__ = __webpack_require__("../../../../../src/app/AdminModule/services/roles.services.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewUsuarioComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NewUsuarioComponent = (function () {
    function NewUsuarioComponent(us, ds, rs) {
        var _this = this;
        this.us = us;
        this.ds = ds;
        this.rs = rs;
        this.usuario = new __WEBPACK_IMPORTED_MODULE_2__UsuarioModule_model_usuario_model__["a" /* Usuario */]();
        ds.getAllDirectorio().subscribe(function (res) {
            _this.directorio = res.datos;
            console.log(res);
        }, function (error) {
            console.log(error);
        });
        rs.getAllRoles().subscribe(function (res) {
            _this.roles = res.datos;
            console.log(res);
        }, function (error) {
            console.log(error);
        });
        // us.getAllClientesSAP().subscribe(res =>{
        //   console.log(res);
        // }, error =>{
        //   console.log(error)
        // })
    }
    NewUsuarioComponent.prototype.guardarUsuario = function () {
        console.log(this.usuario);
        this.us.saveUser(this.usuario, true);
    };
    return NewUsuarioComponent;
}());
NewUsuarioComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'user-new',
        template: __webpack_require__("../../../../../src/app/AdminModule/usuarios/templates/new.usuario.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__UsuarioModule_services_usuario_services__["a" /* UsuarioServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__UsuarioModule_services_usuario_services__["a" /* UsuarioServices */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_directorio_services__["a" /* DirectorioServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_directorio_services__["a" /* DirectorioServices */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__services_roles_services__["a" /* RolesServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_roles_services__["a" /* RolesServices */]) === "function" && _c || Object])
], NewUsuarioComponent);

var _a, _b, _c;
//# sourceMappingURL=new.usuario.component.js.map

/***/ }),

/***/ "../../../../../src/app/AdminModule/usuarios/templates/edit.usuario.component.html":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "../../../../../src/app/AdminModule/usuarios/templates/list.usuario.component.html":
/***/ (function(module, exports) {

module.exports = "<h2>Lista de Usuarios</h2>\r\n\r\n<table class=\"table table-bodered\">\r\n  <thead class=\"thead-light\">\r\n    <tr>\r\n      <th>Id</th>\r\n      <th>Usuario\r\n      <th>Accciones</th>\r\n    </tr>\r\n  </thead>\r\n  <tbody>\r\n    <tr *ngFor=\"let us of usuarios\">\r\n      <td>{{us.id}}</td>\r\n      <td>{{us.username}}</td>\r\n      <td>\r\n        <button class=\"btn\">Ver</button>\r\n        <button class=\"btn btn-primary\">Editar</button>\r\n      </td>\r\n    </tr>\r\n  </tbody>\r\n</table>\r\n"

/***/ }),

/***/ "../../../../../src/app/AdminModule/usuarios/templates/new.usuario.component.html":
/***/ (function(module, exports) {

module.exports = "<h2>Nuevo Usuario</h2>\r\n\r\n\r\n<form #formUser=\"ngForm\">\r\n\r\n  <div class=\"form-group\">\r\n    <label>Username*</label>\r\n    <input name=\"username\" [(ngModel)]=\"usuario.username\" type=\"text\" class=\"form-control\" />\r\n  </div>\r\n\r\n  <div class=\"form-group\">\r\n    <label>Password*</label>\r\n    <input name=\"password\" [(ngModel)]=\"usuario.password\" type=\"password\" class=\"form-control\" />\r\n  </div>\r\n\r\n  <div class=\"form-group\">\r\n    <label>Roles</label>\r\n    <select class=\"form-control\" [(ngModel)]=\"usuario.rol\" multiple name=\"rol\">\r\n      <option *ngFor=\"let r of roles\" value=\"{{r.id}}\">{{r.nombre}}</option>\r\n    </select>\r\n  </div>\r\n\r\n  <div class=\"form-group\">\r\n    <label>Directorio</label>\r\n    <select class=\"form-control\" [(ngModel)]=\"usuario.directorio\" multiple name=\"directorio\">\r\n      <option *ngFor=\"let d of directorio\" value=\"{{ d.id }}\">{{d.email}}</option>\r\n    </select>\r\n  </div>\r\n\r\n\r\n  <!--<div>-->\r\n    <!--<h4>Directorio</h4>-->\r\n\r\n    <!--<ul>-->\r\n      <!--<li *ngFor=\"let d of directorio\">-->\r\n        <!--<input type=\"checkbox\" name=\"prueba\">dsadasdad-->\r\n      <!--</li>-->\r\n    <!--</ul>-->\r\n  <!--</div>-->\r\n  <div class=\"form-group\">\r\n    <input type=\"submit\" value=\"Crear\" class=\"btn btn-primary\" (click)=\"guardarUsuario()\" />\r\n    <button class=\"btn\" routerLink=\"['/admin/usuarios']\">Regresar</button>\r\n  </div>\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/app/AuthModule/auth.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular2_jwt__ = __webpack_require__("../../../../angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angular2_jwt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* unused harmony export authHttpServiceFactory */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



function authHttpServiceFactory(http, options) {
    return new __WEBPACK_IMPORTED_MODULE_1_angular2_jwt__["AuthHttp"](new __WEBPACK_IMPORTED_MODULE_1_angular2_jwt__["AuthConfig"]({
        tokenName: 'x-access-token',
        tokenGetter: (function () { return localStorage.getItem('token'); }),
        globalHeaders: [{ 'Content-Type': 'application/json' }]
    }), http, options);
}
var AuthModule = (function () {
    function AuthModule() {
    }
    return AuthModule;
}());
AuthModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"])({
        providers: [
            {
                provide: __WEBPACK_IMPORTED_MODULE_1_angular2_jwt__["AuthHttp"],
                useFactory: authHttpServiceFactory,
                deps: [__WEBPACK_IMPORTED_MODULE_0__angular_http__["Http"], __WEBPACK_IMPORTED_MODULE_0__angular_http__["RequestOptions"]]
            }
        ]
    })
], AuthModule);

;
//# sourceMappingURL=auth.module.js.map

/***/ }),

/***/ "../../../../../src/app/AuthModule/services/auth-guard.services.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__UsuarioModule_services_usuario_services__ = __webpack_require__("../../../../../src/app/UsuarioModule/services/usuario.services.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuardServices; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuardServices = (function () {
    function AuthGuardServices(us, router) {
        this.us = us;
        this.router = router;
    }
    AuthGuardServices.prototype.canActivate = function () {
        if (this.us.isLogin())
            return true;
        else {
            this.router.navigate(['/login']);
            return false;
        }
    };
    return AuthGuardServices;
}());
AuthGuardServices = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__UsuarioModule_services_usuario_services__["a" /* UsuarioServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__UsuarioModule_services_usuario_services__["a" /* UsuarioServices */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_router__["a" /* Router */]) === "function" && _b || Object])
], AuthGuardServices);

var _a, _b;
//# sourceMappingURL=auth-guard.services.js.map

/***/ }),

/***/ "../../../../../src/app/GeneralModule/components/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FooterComponent = (function () {
    function FooterComponent() {
    }
    return FooterComponent;
}());
FooterComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'footer-app',
        template: __webpack_require__("../../../../../src/app/GeneralModule/templates/footer.component.html"),
        styles: [__webpack_require__("../../../../../src/app/GeneralModule/styles/footer.component.scss")]
    })
], FooterComponent);

//# sourceMappingURL=footer.component.js.map

/***/ }),

/***/ "../../../../../src/app/GeneralModule/components/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__UsuarioModule_services_usuario_services__ = __webpack_require__("../../../../../src/app/UsuarioModule/services/usuario.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_translate__ = __webpack_require__("../../../../ng2-translate/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HeaderComponent = (function () {
    function HeaderComponent(_us, translate) {
        this._us = _us;
        this.translate = translate;
        this.us = _us;
    }
    HeaderComponent.prototype.logout = function () {
        this.us.logout();
    };
    HeaderComponent.prototype.cambiarIdioma = function () {
        if (__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */].idiomaAct == 'es') {
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */].idiomaAct = 'en';
        }
        else {
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */].idiomaAct = 'es';
        }
        this.translate.use(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */].idiomaAct);
    };
    return HeaderComponent;
}());
HeaderComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'header-app',
        template: __webpack_require__("../../../../../src/app/GeneralModule/templates/header.component.html"),
        styles: [__webpack_require__("../../../../../src/app/GeneralModule/styles/header.component.scss")],
        providers: [__WEBPACK_IMPORTED_MODULE_1__UsuarioModule_services_usuario_services__["a" /* UsuarioServices */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__UsuarioModule_services_usuario_services__["a" /* UsuarioServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__UsuarioModule_services_usuario_services__["a" /* UsuarioServices */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_translate__["b" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_translate__["b" /* TranslateService */]) === "function" && _b || Object])
], HeaderComponent);

var _a, _b;
//# sourceMappingURL=header.component.js.map

/***/ }),

/***/ "../../../../../src/app/GeneralModule/components/lista.carro.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_observable_of__ = __webpack_require__("../../../../rxjs/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_observable_of__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_carro_services__ = __webpack_require__("../../../../../src/app/GeneralModule/services/carro.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_translate__ = __webpack_require__("../../../../ng2-translate/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListaCarroComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ListaCarroComponent = (function () {
    function ListaCarroComponent(cs, router, modalService, translate) {
        this.cs = cs;
        this.router = router;
        this.modalService = modalService;
        this.translate = translate;
        this.prodCart$ = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_rxjs_observable_of__["of"])([]);
        this.prodCart = [];
        this.tipoVista = 0;
        this.textoVista = "submenu.lista";
        this.errorMsg = "";
    }
    ListaCarroComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.prodCart$ = this.cs.getProductos();
        this.prodCart$.subscribe(function (value) {
            _this.prodCart = value;
        });
    };
    ListaCarroComponent.prototype.cambiarTipoVista = function () {
        if (this.tipoVista == 0) {
            this.tipoVista = 1;
            this.textoVista = "submenu.grafica";
        }
        else if (this.tipoVista == 1) {
            this.tipoVista = 0;
            this.textoVista = "submenu.lista";
        }
    };
    ListaCarroComponent.prototype.actualizarProducto = function () {
        this.cs.almacenarProducto(this.prodSelect);
        this.prodSelect = null;
        this.modalRef.hide();
    };
    ListaCarroComponent.prototype.anadirProducto = function (prod) {
        if (prod != null) {
            this.prodSelect = prod;
            this.abrirModal();
        }
    };
    ListaCarroComponent.prototype.abrirModal = function () {
        var _this = this;
        if (this.prodSelect.cantidad >= this.prodSelect.multiplo && (this.prodSelect.cantidad % this.prodSelect.multiplo) == 0) {
            this.errorMsg = '';
        }
        else {
            this.translate.get('prod.errorMultiplo').subscribe(function (val) {
                _this.errorMsg = val + _this.prodSelect.multiplo;
                _this.prodSelect.cantidad = _this.prodSelect.multiplo;
            });
        }
        this.modalRef = this.modalService.show(this.templateModal);
    };
    ListaCarroComponent.prototype.quitarProducto = function (p) {
        this.cs.eliminarProducto(p);
    };
    ListaCarroComponent.prototype.vaciarCarrito = function () {
        this.cs.vaciarCarrito();
    };
    ListaCarroComponent.prototype.aprobarPedido = function () {
        this.router.navigate(['/requerimiento']);
    };
    return ListaCarroComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('confirmarCompra'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"]) === "function" && _a || Object)
], ListaCarroComponent.prototype, "templateModal", void 0);
ListaCarroComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "lista-carro",
        template: __webpack_require__("../../../../../src/app/GeneralModule/templates/lista.carro.component.html"),
        styles: [__webpack_require__("../../../../../src/app/GeneralModule/styles/lista.carro.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_carro_services__["a" /* CarroServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_carro_services__["a" /* CarroServices */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap__["c" /* BsModalService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap__["c" /* BsModalService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5_ng2_translate__["b" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_ng2_translate__["b" /* TranslateService */]) === "function" && _e || Object])
], ListaCarroComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=lista.carro.component.js.map

/***/ }),

/***/ "../../../../../src/app/GeneralModule/components/mini.carro.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_carro_services__ = __webpack_require__("../../../../../src/app/GeneralModule/services/carro.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__ = __webpack_require__("../../../../rxjs/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MiniCarroComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MiniCarroComponent = (function () {
    function MiniCarroComponent(carroService) {
        var _this = this;
        this.carroService = carroService;
        this.prodCart$ = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__["of"])([]);
        this.prodCart = [];
        this.prodCart$ = this.carroService.getProductos();
        this.prodCart$.subscribe(function (value) {
            _this.prodCart = value;
        });
    }
    return MiniCarroComponent;
}());
MiniCarroComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'mini-carro',
        template: __webpack_require__("../../../../../src/app/GeneralModule/templates/mini.carro.component.html"),
        styles: [__webpack_require__("../../../../../src/app/GeneralModule/styles/mini.carro.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_carro_services__["a" /* CarroServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_carro_services__["a" /* CarroServices */]) === "function" && _a || Object])
], MiniCarroComponent);

var _a;
//# sourceMappingURL=mini.carro.component.js.map

/***/ }),

/***/ "../../../../../src/app/GeneralModule/components/requerimiento.exp.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_requerimiento_model__ = __webpack_require__("../../../../../src/app/GeneralModule/models/requerimiento.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_carro_services__ = __webpack_require__("../../../../../src/app/GeneralModule/services/carro.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_globals__ = __webpack_require__("../../../../../src/app/GeneralModule/services/globals.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_translate__ = __webpack_require__("../../../../ng2-translate/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequerimientoExpComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RequerimientoExpComponent = (function () {
    function RequerimientoExpComponent(cs, GS, router, translate) {
        var _this = this;
        this.cs = cs;
        this.GS = GS;
        this.router = router;
        this.translate = translate;
        this.req = new __WEBPACK_IMPORTED_MODULE_1__models_requerimiento_model__["a" /* Requerimiento */]();
        this.mismoConsigNoti = "";
        this.codInco = [];
        this.pLlegadas = [];
        this.pSalidas = [];
        this.modalidadEmbarque = [];
        this.tipoContenedor = [];
        this.errors = "";
        this.contenedorHabilitado = true;
        this.tipoDocumentos = [
            // {name:"Certificado Origen", value:'Certificado Origen', checked:false },
            // {name:"Certificado Transaccion Organico", value:'Certificado Transaccion Organico', checked:false },
            // {name:"Certificado Libre Venta", value:'Certificado Libre Venta', checked:false },
            // {name:"Apostilla", value:'Apostilla', checked:false },
            // {name:"Otros Requerimientos Especiales", value:'Otros Requerimientos Especiales', checked:false }
            { name: "req.cerOri", value: 'Certificado Origen', checked: false },
            { name: "req.transOrg", value: 'Certificado Transaccion Organico', checked: false },
            { name: "req.libreVent", value: 'Certificado Libre Venta', checked: false },
            { name: "req.apostilla", value: 'Apostilla', checked: false },
            { name: "req.otrosReqEsp", value: 'Otros Requerimientos Especiales', checked: false }
        ];
        this.reqxProd = [];
        var prodCart$ = this.cs.getProductos();
        var productos = [];
        prodCart$.subscribe(function (value) {
            productos = value;
        });
        for (var _i = 0, productos_1 = productos; _i < productos_1.length; _i++) {
            var p = productos_1[_i];
            this.reqxProd.push({
                nombre: p.nombre,
                original: p,
                prod: false,
                cajaMaster: false,
                pallet: false,
                observaciones: ""
            });
        }
        this.GS.devolverCodigos().subscribe(function (res) {
            // console.log(res);
            _this.codigos = res;
            _this.codInco = _this.devolverCodFiltrado(2);
            _this.pLlegadas = _this.devolverCodFiltrado(5);
            _this.pSalidas = _this.devolverCodFiltrado(4);
            _this.modalidadEmbarque = _this.devolverCodFiltrado(3);
            _this.tipoContenedor = _this.devolverCodFiltrado(6);
        }, function (error) {
            console.log(error);
        });
        this.ActivarFletePrepagado();
    }
    RequerimientoExpComponent.prototype.ngOnInit = function () {
    };
    RequerimientoExpComponent.prototype.devolverCodIncoterm = function () {
        return this.codigos.filter(function (cod) {
            if (cod.tipo == 2)
                return true;
            else
                return false;
        });
    };
    RequerimientoExpComponent.prototype.devolverCodFiltrado = function (tipo) {
        return this.codigos.filter(function (cod) {
            if (cod.tipo == tipo)
                return true;
            else
                return false;
        });
    };
    Object.defineProperty(RequerimientoExpComponent.prototype, "selectedDocuments", {
        get: function () {
            return this.tipoDocumentos
                .filter(function (doc) { return doc.checked; })
                .map(function (doc) { return doc.value; });
        },
        enumerable: true,
        configurable: true
    });
    RequerimientoExpComponent.prototype.ajusteDocumentos = function (event) {
        this.req.docAdicional = this.selectedDocuments;
        this.validarOtrosReq();
    };
    RequerimientoExpComponent.prototype.cambioEnReqProducto = function (event) {
        for (var _i = 0, _a = this.reqxProd; _i < _a.length; _i++) {
            var p = _a[_i];
            p.prod = event.target.checked;
        }
    };
    RequerimientoExpComponent.prototype.cambioEnReqCajaMaster = function (event) {
        for (var _i = 0, _a = this.reqxProd; _i < _a.length; _i++) {
            var p = _a[_i];
            p.cajaMaster = event.target.checked;
        }
    };
    RequerimientoExpComponent.prototype.cambioEnReqPallets = function (event) {
        for (var _i = 0, _a = this.reqxProd; _i < _a.length; _i++) {
            var p = _a[_i];
            p.pallet = event.target.checked;
        }
    };
    RequerimientoExpComponent.prototype.finalizarOrden = function () {
        var _this = this;
        var error = this.req.validarRequerimiento();
        this.errors = error;
        console.log(error);
        var cantidadErrores = Object.keys(error).length;
        console.log(cantidadErrores);
        var cliente = localStorage.getItem("cliente");
        var cantidadDePedidos = 0;
        if (cantidadErrores == 0) {
            var datos = {
                orderList: []
            };
            var prodCafe = this.filtrarProductos(true);
            var prodMerc = this.filtrarProductos(false);
            if (prodCafe.length > 0) {
                var ordenCafe = new __WEBPACK_IMPORTED_MODULE_1__models_requerimiento_model__["b" /* Order */]("SP8000", this.req, this.GS, this.tipoDocumentos);
                ordenCafe.agregarProductos(prodCafe);
                datos.orderList.push({ order: ordenCafe.serialize() });
                cantidadDePedidos++;
                console.log(ordenCafe);
            }
            if (prodMerc.length > 0) {
                var ordenMerc = new __WEBPACK_IMPORTED_MODULE_1__models_requerimiento_model__["b" /* Order */]("SP8001", this.req, this.GS, this.tipoDocumentos);
                ordenMerc.agregarProductos(prodMerc);
                datos.orderList.push({ order: ordenMerc.serialize() });
                cantidadDePedidos++;
                console.log(ordenMerc);
            }
            this.cs.enviarOrden({ datos: datos, cliente: cliente }).subscribe(function (result) {
                console.log(result);
                if (result.error === null) {
                    var documentos = [];
                    var doc_1 = result.result.return.responseBody.summaryOrderList.summaryOrder;
                    console.log(doc_1);
                    if (doc_1.length > cantidadDePedidos) {
                        var _loop_1 = function (i) {
                            if (i != doc_1.length - 1) {
                                _this.translate.get('req.compromisoVenta').subscribe(function (text) {
                                    _this.GS.agregarMensaje(text + " " + doc_1[i].salesDocument);
                                });
                            }
                            else {
                                _this.translate.get('req.instruccionEmbarque').subscribe(function (text) {
                                    _this.GS.agregarMensaje(text + " " + doc_1[i].salesDocument);
                                });
                            }
                        };
                        for (var i = 0; i < doc_1.length; i++) {
                            _loop_1(i);
                        }
                        console.log(_this.GS.flagsMessage);
                        // for(let d of doc){
                        //   this.GS.agregarMensaje("Se genero el documento numero " + d.salesDocument);
                        //   documentos.push(d.salesDocument);
                        //
                        // }
                        _this.cs.vaciarCarrito();
                        _this.router.navigate(['/']);
                    }
                }
            }, function (error) {
                console.log(error);
            });
        }
    };
    RequerimientoExpComponent.prototype.filtrarProductos = function (esCafe) {
        if (esCafe)
            return this.reqxProd.filter(function (p) { return p.original.sector === "CF"; });
        else
            return this.reqxProd.filter(function (p) { return p.original.sector !== "CF"; });
    };
    RequerimientoExpComponent.prototype.cambioTermino = function (event) {
        console.log(event);
        switch (event) {
            case "CFR":
                this.ActivarFletePrepagado();
                break;
            case "CIF":
                this.ActivarFletePrepagado();
                break;
            case "CIP":
                this.ActivarFletePrepagado();
                break;
            case "CPT":
                this.ActivarFletePrepagado();
                break;
            case "DAP":
                this.ActivarFletePrepagado();
                break;
            case "DDP":
                this.ActivarFletePrepagado();
                break;
            case "FCA":
                this.ActivarFleteCobrar();
                break;
            case "FOB":
                this.ActivarFleteCobrar();
                break;
            default:
                this.ActivarFletePrepagado();
                break;
        }
    };
    RequerimientoExpComponent.prototype.ActivarFleteCobrar = function () {
        var _this = this;
        this.translate.get("req.fleteCobrar").subscribe(function (text) {
            _this.req.dtNegociacion.flete.nombre = text;
            _this.req.dtNegociacion.flete.valor = "C";
        });
    };
    RequerimientoExpComponent.prototype.ActivarFletePrepagado = function () {
        var _this = this;
        this.translate.get("req.fletePrepagado").subscribe(function (text) {
            _this.req.dtNegociacion.flete.nombre = text;
            _this.req.dtNegociacion.flete.valor = "P";
        });
    };
    RequerimientoExpComponent.prototype.ajustarInfoConsigNoti = function (event) {
        if (event)
            this.req.notificado.copiarInformacion(this.req.consignatario);
        else
            this.req.notificado.limpiarInformacion();
    };
    RequerimientoExpComponent.prototype.evaluarEmisionBL = function () {
        if (!(this.req.dtNegociacion.incoterm == "DAP" || this.req.dtNegociacion.incoterm == "DDP")) {
            return true;
        }
        else {
            return null;
        }
    };
    RequerimientoExpComponent.prototype.evaluarAgenciaCarga = function () {
        if (this.req.dtNegociacion.incoterm == "DAP" || this.req.dtNegociacion.incoterm == "DDP") {
            this.req.agenciaCarga.activo = false;
            return true;
        }
        else {
            this.req.agenciaCarga.activo = true;
            return null;
        }
    };
    RequerimientoExpComponent.prototype.validarObsFactura = function (event) {
        if (this.req.llevaObservaciones)
            return true;
        else
            return null;
    };
    RequerimientoExpComponent.prototype.validarOtrosReq = function () {
        var result = this.req.docAdicional.filter(function (doc) { return doc == "Otros Requerimientos Especiales"; });
        if (result.length > 0)
            return true;
        else
            return null;
    };
    RequerimientoExpComponent.prototype.puertoSalidaActivado = function () {
        return (this.req.dtNegociacion.flete.valor == "P") ? true : null;
    };
    RequerimientoExpComponent.prototype.puertoLlegadaActivado = function () {
        return (this.req.dtNegociacion.flete.valor == "C") ? true : null;
    };
    RequerimientoExpComponent.prototype.onModalidadEnbarqueChange = function (event) {
        if (event.target.value == "A1")
            this.contenedorHabilitado = false;
        else
            this.contenedorHabilitado = true;
    };
    return RequerimientoExpComponent;
}());
RequerimientoExpComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'requerimiento',
        template: __webpack_require__("../../../../../src/app/GeneralModule/templates/requerimiento.exp.component.html"),
        styles: [__webpack_require__("../../../../../src/app/GeneralModule/styles/requerimiento.exp.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_carro_services__["a" /* CarroServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_carro_services__["a" /* CarroServices */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_globals__["a" /* GlobalServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_globals__["a" /* GlobalServices */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5_ng2_translate__["b" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_ng2_translate__["b" /* TranslateService */]) === "function" && _d || Object])
], RequerimientoExpComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=requerimiento.exp.component.js.map

/***/ }),

/***/ "../../../../../src/app/GeneralModule/general.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_header_component__ = __webpack_require__("../../../../../src/app/GeneralModule/components/header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_footer_component__ = __webpack_require__("../../../../../src/app/GeneralModule/components/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ProductoModule_producto_module__ = __webpack_require__("../../../../../src/app/ProductoModule/producto.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_lista_carro_component__ = __webpack_require__("../../../../../src/app/GeneralModule/components/lista.carro.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_mini_carro_component__ = __webpack_require__("../../../../../src/app/GeneralModule/components/mini.carro.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__general_routing_module__ = __webpack_require__("../../../../../src/app/GeneralModule/general.routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_requerimiento_exp_component__ = __webpack_require__("../../../../../src/app/GeneralModule/components/requerimiento.exp.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_translate__ = __webpack_require__("../../../../ng2-translate/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var GeneralModule = (function () {
    function GeneralModule() {
    }
    return GeneralModule;
}());
GeneralModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["k" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_8__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_4__ProductoModule_producto_module__["a" /* ProductoModule */],
            __WEBPACK_IMPORTED_MODULE_7__general_routing_module__["a" /* GeneralRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_10_ngx_bootstrap__["d" /* PopoverModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_10_ngx_bootstrap__["b" /* AlertModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_11_ng2_translate__["a" /* TranslateModule */]
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__components_header_component__["a" /* HeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_3__components_footer_component__["a" /* FooterComponent */],
            __WEBPACK_IMPORTED_MODULE_5__components_lista_carro_component__["a" /* ListaCarroComponent */],
            __WEBPACK_IMPORTED_MODULE_6__components_mini_carro_component__["a" /* MiniCarroComponent */],
            __WEBPACK_IMPORTED_MODULE_9__components_requerimiento_exp_component__["a" /* RequerimientoExpComponent */]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_1__angular_common__["l" /* DatePipe */]],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__components_header_component__["a" /* HeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_3__components_footer_component__["a" /* FooterComponent */],
            __WEBPACK_IMPORTED_MODULE_6__components_mini_carro_component__["a" /* MiniCarroComponent */]
        ]
    })
], GeneralModule);

//# sourceMappingURL=general.module.js.map

/***/ }),

/***/ "../../../../../src/app/GeneralModule/general.routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_lista_carro_component__ = __webpack_require__("../../../../../src/app/GeneralModule/components/lista.carro.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_requerimiento_exp_component__ = __webpack_require__("../../../../../src/app/GeneralModule/components/requerimiento.exp.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__AuthModule_services_auth_guard_services__ = __webpack_require__("../../../../../src/app/AuthModule/services/auth-guard.services.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var generalRoutes = [
    { path: "carrito", component: __WEBPACK_IMPORTED_MODULE_2__components_lista_carro_component__["a" /* ListaCarroComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_4__AuthModule_services_auth_guard_services__["a" /* AuthGuardServices */]] },
    { path: "requerimiento", component: __WEBPACK_IMPORTED_MODULE_3__components_requerimiento_exp_component__["a" /* RequerimientoExpComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_4__AuthModule_services_auth_guard_services__["a" /* AuthGuardServices */]] }
];
var GeneralRoutingModule = (function () {
    function GeneralRoutingModule() {
    }
    return GeneralRoutingModule;
}());
GeneralRoutingModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */].forChild(generalRoutes)
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */]
        ]
    })
], GeneralRoutingModule);

//# sourceMappingURL=general.routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/GeneralModule/models/requerimiento.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ts_serializer__ = __webpack_require__("../../../../ts-serializer/dist/ts-serializer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ts_serializer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ts_serializer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_globals__ = __webpack_require__("../../../../../src/app/GeneralModule/services/globals.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Requerimiento; });
/* unused harmony export DatosNegociacion */
/* unused harmony export DatosDespacho */
/* unused harmony export DetalleAgenciamiento */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Order; });
/* unused harmony export OrderHeader */
/* unused harmony export OrderDetail */
/* unused harmony export TypeText */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Requerimiento = (function () {
    function Requerimiento() {
        // datos de negociacion
        this.dtNegociacion = new DatosNegociacion();
        // datos de despacho
        this.dtDespacho = new DatosDespacho();
        // datos de agenciamiento
        this.serviceContract = "";
        this.agenciaCarga = new DetalleAgenciamiento();
        this.consignatario = new DetalleAgenciamiento();
        this.notificado = new DetalleAgenciamiento();
        // documentos adicionales para nacionalizacion en destino
        this.docAdicional = [];
        this.otrosRequerimientos = "";
        this.llevaObservaciones = false;
        this.observaciones = "";
        // marcacion especial
        this.marcacionEspecial = false;
        this.errorDoc = {};
    }
    Requerimiento.prototype.validarRequerimiento = function () {
        this.errorDoc = {};
        this.agregarError(this.dtNegociacion.validar());
        this.agregarError(this.dtDespacho.validar(this.dtNegociacion.flete.valor == "P" ? 1 : 0));
        this.agregarError(this.agenciaCarga.validar("ac", 1));
        this.agregarError(this.consignatario.validar("co", 2));
        this.agregarError(this.notificado.validar("no", 3));
        if (this.agenciaCarga.activo) {
            if (this.serviceContract == "") {
                this.errorDoc["servicesContract"] = true;
            }
        }
        return this.errorDoc;
    };
    Requerimiento.prototype.agregarError = function (err) {
        for (var key in err)
            this.errorDoc[key] = err[key];
    };
    return Requerimiento;
}());

var DatosNegociacion = (function () {
    function DatosNegociacion() {
        this.incoterm = "";
        this.emisionBL = "";
        this.direccionMercancia = "";
        this.codigoZIP = "";
        this.contacto = "";
        this.telem = "";
        this.flete = null;
        this.activo = true;
        this.error = {};
        this.flete = {
            nombre: "Flete Prepagado",
            valor: "P"
        };
    }
    DatosNegociacion.prototype.validar = function () {
        this.error = {};
        this.activo = (this.incoterm == 'DAP' || this.incoterm == "DDP");
        if (this.incoterm === "")
            this.error["incoterm"] = true;
        if (this.emisionBL === "")
            this.error["emisionBL"] = true;
        if (this.activo) {
            if (this.direccionMercancia === "")
                this.error["direccionMercancia"] = true;
            if (this.codigoZIP === "")
                this.error["codigoZIP"] = true;
            if (this.contacto === "")
                this.error["contacto"] = true;
            if (this.telem === "")
                this.error["telem"] = true;
        }
        return this.error;
    };
    DatosNegociacion.prototype.convertirTexto = function () {
        if (!this.activo)
            return "";
        var result = "";
        result += "Direccion de entraga de la M/cia: " + this.direccionMercancia + " ";
        result += "Codigo ZIP: " + this.codigoZIP + " ";
        result += "Contacto: " + this.contacto + " ";
        result += "Telefono - email: " + this.telem + " ";
        return result;
    };
    return DatosNegociacion;
}());

var DatosDespacho = (function () {
    function DatosDespacho() {
        this.modalidadEmbarque = "";
        this.fechaPosibleDespacho = "";
        this.tipoContenedor = "";
        this.valorContenedor = "";
        this.puertoSalida = "";
        this.puertoLlegada = "";
        this.error = {};
    }
    DatosDespacho.prototype.validar = function (tipoFlete) {
        this.error = {};
        if (this.modalidadEmbarque === "")
            this.error["modalidadEmbarque"] = true;
        if (this.fechaPosibleDespacho === "")
            this.error["fechaPosibleDespacho"] = true;
        if (this.modalidadEmbarque != "A1") {
            if (this.tipoContenedor === "")
                this.error["tipoContenedor"] = true;
            if (this.valorContenedor === "")
                this.error["valorContenedor"] = true;
        }
        // para el caso de flete prepagdo
        if (tipoFlete == 1) {
            if (this.puertoLlegada === "")
                this.error["puertoLlegada"] = true;
        }
        else {
            if (this.puertoSalida === "")
                this.error["puertoSalida"] = true;
        }
        return this.error;
    };
    return DatosDespacho;
}());

var DetalleAgenciamiento = (function () {
    function DetalleAgenciamiento() {
        this.razonSocial = "";
        this.nit = "";
        this.direccion = "";
        this.telefono = "";
        this.ciudad = "";
        this.direccionEnvio = "";
        this.contacto = "";
        this.activo = true;
        this.error = {};
    }
    DetalleAgenciamiento.prototype.convertirTexto = function (agencia) {
        if (!this.activo)
            return "";
        var result = "";
        result += "Razon Social: " + this.razonSocial + " ";
        result += "Contacto: " + this.contacto + " ";
        result += "Telefono - Email: " + this.telefono + " ";
        if (agencia)
            return result;
        result += "Nit: " + this.nit + " ";
        result += "Direccion: " + this.direccion + " ";
        result += "Ciudad: " + this.ciudad + " ";
        result += "Direccion Envio: " + this.direccionEnvio + " ";
        return result;
    };
    DetalleAgenciamiento.prototype.validar = function (prefix, tipo) {
        this.error = {};
        if (!this.activo)
            return this.error;
        if (this.razonSocial === "")
            this.error[prefix + "razonSocial"] = true;
        if (this.telefono === "")
            this.error[prefix + "telefono"] = true;
        // 1 para agencia de carga
        if (tipo == 1) {
            if (this.contacto === "")
                this.error[prefix + "contacto"] = true;
        }
        else {
            if (this.nit === "")
                this.error[prefix + "nit"] = true;
            if (this.direccion === "")
                this.error[prefix + "direccion"] = true;
            if (this.ciudad === "")
                this.error[prefix + "ciudad"] = true;
            if (tipo == 2) {
                if (this.direccionEnvio === "")
                    this.error[prefix + "direccionEnvio"] = true;
            }
        }
        return this.error;
    };
    DetalleAgenciamiento.prototype.copiarInformacion = function (dtAgenciamiento) {
        this.razonSocial = dtAgenciamiento.razonSocial;
        this.nit = dtAgenciamiento.nit;
        this.direccion = dtAgenciamiento.direccion;
        this.telefono = dtAgenciamiento.telefono;
        this.ciudad = dtAgenciamiento.ciudad;
    };
    DetalleAgenciamiento.prototype.limpiarInformacion = function () {
        this.razonSocial = "";
        this.nit = "";
        this.direccion = "";
        this.telefono = "";
        this.ciudad = "";
    };
    return DetalleAgenciamiento;
}());

var Order = (function (_super) {
    __extends(Order, _super);
    function Order(doc, req, GS, tipoDoc) {
        var _this = _super.call(this) || this;
        _this.orderHeader = null;
        _this.orderDetails = [];
        _this.indexPos = 10;
        _this.orderHeader = new OrderHeader(doc, req, GS, tipoDoc);
        return _this;
    }
    Order.prototype.agregarProductos = function (prods) {
        for (var _i = 0, prods_1 = prods; _i < prods_1.length; _i++) {
            var pro = prods_1[_i];
            var od = new OrderDetail(this.orderHeader.requestNumber, pro, this.indexPos, 2);
            this.orderDetails.push({ orderDetail: od });
            this.indexPos += 10;
        }
    };
    return Order;
}(__WEBPACK_IMPORTED_MODULE_0_ts_serializer__["Serializable"]));
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_ts_serializer__["SerializeProperty"])({}),
    __metadata("design:type", OrderHeader)
], Order.prototype, "orderHeader", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_ts_serializer__["SerializeProperty"])({}),
    __metadata("design:type", Object)
], Order.prototype, "orderDetails", void 0);
Order = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_ts_serializer__["Serialize"])({}),
    __metadata("design:paramtypes", [String, Requerimiento, typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_globals__["a" /* GlobalServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_globals__["a" /* GlobalServices */]) === "function" && _a || Object, Array])
], Order);

var OrderHeader = (function () {
    function OrderHeader(doc, req, GS, tipoDoc) {
        this.requestNumber = "SP8000";
        this.salesOrg = "";
        this.channel = "";
        this.sector = "00";
        this.salesOffice = "FQI1"; // preguntar
        this.vendorGroup = "OC1"; // preguntar
        this.docType = "ZCVP"; //fijo
        this.deliveryDate = "2017-12-30T00:00:00"; // fecha en formato iso
        this.maxDeliveryDate = "2017-12-30T00:00:00"; // fecha en formato iso
        this.currency = "USD"; // fijo
        this.containerNumber = 2; // tipo de contenedor
        this.containerClass = "EC3";
        this.sourceCode = ""; // cliente
        this.targetCode = ""; // cliente
        this.transportation = "A1";
        this.incoterms = "";
        this.outgoingPort = "B2";
        this.incomingPort = "B2";
        this.textTypeList = []; // preguntar si son obs
        this.requestNumber = doc;
        var cliente = localStorage.getItem("cliente");
        this.channel = localStorage.getItem('canal');
        this.salesOrg = localStorage.getItem('orgVenta');
        var destinatario = localStorage.getItem('destinatario');
        this.sourceCode = destinatario;
        this.targetCode = destinatario;
        this.incoterms = req.dtNegociacion.incoterm;
        // let fecha = moment(req.dtDespacho.fechaPosibleDespacho);
        // console.log(fecha.toISOString());
        // console.log(req.dtDespacho.fechaPosibleDespacho)
        this.deliveryDate = GS.dateToISO(req.dtDespacho.fechaPosibleDespacho);
        this.maxDeliveryDate = GS.dateToISO(req.dtDespacho.fechaPosibleDespacho);
        if (req.dtDespacho.modalidadEmbarque != "A1")
            this.containerClass = req.dtDespacho.tipoContenedor;
        this.outgoingPort = req.dtDespacho.puertoSalida;
        this.incomingPort = req.dtDespacho.puertoLlegada;
        this.transportation = req.dtDespacho.modalidadEmbarque;
        this.crearTextos(req, tipoDoc);
    }
    OrderHeader.prototype.crearTextos = function (req, tipoDoc) {
        var textType = new TypeText(this.requestNumber, 0, "ZI01", req.dtNegociacion.emisionBL);
        this.textTypeList.push({ textType: textType });
        // textType = new TypeText(this.requestNumber, 0, "ZI02", req.dtDespacho.modalidadEmbarque);
        // this.textTypeList.push({textType:textType});
        textType = new TypeText(this.requestNumber, 0, "ZI03", req.serviceContract);
        this.textTypeList.push({ textType: textType });
        var docSelec = tipoDoc.filter(function (d) { return d.checked == true; });
        var docText = "";
        for (var _i = 0, docSelec_1 = docSelec; _i < docSelec_1.length; _i++) {
            var d = docSelec_1[_i];
            if (d.name != "Otros Requerimientos Especiales")
                docText += d.value + " ";
            else
                docText += req.otrosRequerimientos + " ";
        }
        textType = new TypeText(this.requestNumber, 0, "ZI04", docText);
        this.textTypeList.push({ textType: textType });
        textType = new TypeText(this.requestNumber, 0, "ZI05", req.observaciones);
        this.textTypeList.push({ textType: textType });
        //
        // textType = new TypeText(this.requestNumber, 0, "ZI06", "Services Contract" + req.serviceContract);
        // this.textTypeList.push({textType:textType});
        // textType = new TypeText(this.requestNumber, 0, "ZI07", "Services Contract" + req.serviceContract);
        // this.textTypeList.push({textType:textType});
        if (req.dtDespacho.modalidadEmbarque != "A1") {
            textType = new TypeText(this.requestNumber, 0, "ZI08", req.dtDespacho.valorContenedor);
            this.textTypeList.push({ textType: textType });
        }
        //
        // textType = new TypeText(this.requestNumber, 0, "ZI09", "Services Contract" + req.serviceContract);
        // this.textTypeList.push({textType:textType});
        // textType = new TypeText(this.requestNumber, 0, "ZI10", "Services Contract" + req.serviceContract);
        // this.textTypeList.push({textType:textType});
        //
        // textType = new TypeText(this.requestNumber, 0, "ZI11", "Services Contract" + req.serviceContract);
        // this.textTypeList.push({textType:textType});
        textType = new TypeText(this.requestNumber, 0, "ZI12", req.agenciaCarga.convertirTexto(true));
        this.textTypeList.push({ textType: textType });
        textType = new TypeText(this.requestNumber, 0, "ZI13", "Consignatario " + req.consignatario.convertirTexto(false));
        this.textTypeList.push({ textType: textType });
        textType = new TypeText(this.requestNumber, 0, "ZI14", "Notificado " + req.notificado.convertirTexto(false));
        this.textTypeList.push({ textType: textType });
        // textType = new TypeText(this.requestNumber, 0, "ZI15", "Doc adicional" + req.serviceContract);
        // this.textTypeList.push({textType:textType});
        // textType = new TypeText(this.requestNumber, 0, "ZI16", "Services Contract" + req.serviceContract);
        // this.textTypeList.push({textType:textType});
        textType = new TypeText(this.requestNumber, 0, "ZI17", "Services Contract" + req.dtNegociacion.convertirTexto());
        this.textTypeList.push({ textType: textType });
    };
    return OrderHeader;
}());

var OrderDetail = (function () {
    function OrderDetail(doc, producto, position, pallet) {
        this.requestNumber = "SP8000"; // consecutivo doc
        this.positionNumber = 10;
        this.material = ""; //sku producto
        this.center = ""; // center producto
        this.warehouse = ""; // preguntar
        this.unitType = "UN"; // un producto
        this.quantity = 0; // cantidad comprada
        this.palletNumber = 0; // preguntar
        this.textTypeList = []; //preguntar si son obs
        this.requestNumber = doc;
        this.positionNumber = position;
        this.material = producto.original.sku;
        this.center = localStorage.getItem('centro');
        this.quantity = producto.original.cantidad;
        this.palletNumber = pallet;
        this.warehouse = localStorage.getItem('almacen');
        // this.unitType = producto.unidad;
        var textType = new TypeText(doc, position, "ZI01", producto.cajaMaster ? "Si" : "No");
        this.textTypeList.push({ textType: textType });
        textType = new TypeText(doc, position, "ZI02", producto.pallet ? "Si" : "No");
        this.textTypeList.push({ textType: textType });
        textType = new TypeText(doc, position, "ZI03", producto.observaciones);
        this.textTypeList.push({ textType: textType });
        textType = new TypeText(doc, position, "ZI04", producto.prod ? "Si" : "No");
        this.textTypeList.push({ textType: textType });
    }
    return OrderDetail;
}());

var TypeText = (function () {
    function TypeText(doc, position, id, obs) {
        this.requestNumber = "";
        this.positionNumber = 10;
        this.textId = "";
        this.textline = "";
        this.requestNumber = doc;
        // if(cabecera)
        //   this.textId = "ZI01";
        // else {
        //   this.textId = "ZI03";
        //   this.positionNumber= position;
        // }
        this.textId = id;
        this.positionNumber = position;
        this.textline = obs;
    }
    return TypeText;
}());

var _a;
//# sourceMappingURL=requerimiento.model.js.map

/***/ }),

/***/ "../../../../../src/app/GeneralModule/services/carro.services.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__ = __webpack_require__("../../../../angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_jwt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__globals__ = __webpack_require__("../../../../../src/app/GeneralModule/services/globals.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarroServices; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CarroServices = (function () {
    function CarroServices(globals, authHttp) {
        var _this = this;
        this.globals = globals;
        this.authHttp = authHttp;
        this.productos = [];
        this.prodCartSubject = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]([]);
        this.cantProd = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](0);
        this.prodCartSubject.subscribe(function (value) {
            _this.productos = value;
        });
        //localStorage.clear();
        var carrito = JSON.parse(localStorage.getItem("productos"));
        if (carrito == null)
            carrito = [];
        this.productos = carrito;
        this.prodCartSubject.next(this.productos);
    }
    CarroServices.prototype.almacenarProducto = function (prod) {
        var clone = Object.assign(prod);
        this.productos = this.prodCartSubject.getValue();
        var result = this.comprobarProducto(clone);
        if (result) {
            this.actualizarProducto(clone, result);
        }
        else {
            this.productos.push(clone);
            // this.prodCartSubject.next([this.productos, clone]);
        }
        this.prodCartSubject.next(this.productos);
        this.cantProd.next(this.productos.length);
        this.persistirCarrito();
    };
    CarroServices.prototype.getProductos = function () {
        return this.prodCartSubject;
    };
    CarroServices.prototype.comprobarProducto = function (prod) {
        if (this.productos == null)
            return false;
        return this.productos.find(function (p) {
            return p.id === prod.id;
        });
    };
    CarroServices.prototype.persistirCarrito = function () {
        var carrito = JSON.stringify(this.productos);
        localStorage.setItem("productos", carrito);
    };
    CarroServices.prototype.devolverCantProductos = function () {
        return this.productos.length;
    };
    CarroServices.prototype.actualizarProducto = function (prod, prodAlm) {
        prodAlm.cantidad = prod.cantidad;
    };
    CarroServices.prototype.eliminarProducto = function (prod) {
        var p = this.productos.find(function (p) {
            return p.id === prod.id;
        });
        if (p) {
            var indice = this.productos.indexOf(p);
            this.productos.splice(indice, 1);
            this.prodCartSubject.next(this.productos);
            this.cantProd.next(this.productos.length);
            this.persistirCarrito();
        }
    };
    CarroServices.prototype.vaciarCarrito = function () {
        this.productos = [];
        this.prodCartSubject.next(this.productos);
        this.cantProd.next(this.productos.length);
        this.persistirCarrito();
    };
    CarroServices.prototype.cantTotalCajas = function () {
        var ctCajas = 0;
        for (var _i = 0, _a = this.productos; _i < _a.length; _i++) {
            var p = _a[_i];
            ctCajas += p.cantCajas * (p.cantidad / p.multiplo);
        }
        return ctCajas;
    };
    CarroServices.prototype.cantTotalTendidos = function () {
        var ctTendidos = 0;
        for (var _i = 0, _a = this.productos; _i < _a.length; _i++) {
            var p = _a[_i];
            ctTendidos += p.cantTendidos * (p.cantidad / p.multiplo);
        }
        return ctTendidos;
    };
    CarroServices.prototype.cantTotalPallets = function () {
        var ctPallets = 0;
        for (var _i = 0, _a = this.productos; _i < _a.length; _i++) {
            var p = _a[_i];
            ctPallets += p.cantPallets * (p.cantidad / p.multiplo);
        }
        return ctPallets;
    };
    CarroServices.prototype.cantTotal = function () {
        var total = 0;
        for (var _i = 0, _a = this.productos; _i < _a.length; _i++) {
            var p = _a[_i];
            total += p.cantidad * p.valor;
        }
        return total;
    };
    CarroServices.prototype.devolverTipoCafe = function () {
        return this.productos.filter(function (p) { return p.sector === "CF"; });
    };
    CarroServices.prototype.devolverTipoMercadeo = function () {
        return this.productos.filter(function (p) { return p.sector !== "CF"; });
    };
    CarroServices.prototype.enviarOrden = function (orden) {
        return this.authHttp.post(this.globals.urlServer + "/salesOrder", orden).map(function (res) { return res.json(); });
    };
    return CarroServices;
}());
CarroServices = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__globals__["a" /* GlobalServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__globals__["a" /* GlobalServices */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__["AuthHttp"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__["AuthHttp"]) === "function" && _b || Object])
], CarroServices);

var _a, _b;
//# sourceMappingURL=carro.services.js.map

/***/ }),

/***/ "../../../../../src/app/GeneralModule/services/globals.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__ = __webpack_require__("../../../../angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_jwt__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalServices; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GlobalServices = (function () {
    function GlobalServices(datePipe, authHttp) {
        this.datePipe = datePipe;
        this.authHttp = authHttp;
        this.uuid = "50ff93b6-a7b6-11e7-abc4-cec278b6b50a";
        // public urlServer: string = "http://192.168.247.4:4700";
        this.urlServer = "http://192.168.17.129:4700";
        //   public urlServer: string = "http://localhost:4700";
        this.flagsMessage = [];
    }
    GlobalServices.prototype.dateToISO = function (fecha) {
        var dato = this.datePipe.transform(fecha, "yyyy-MM-dd HH:mm:ss");
        dato = dato.replace(" ", "T");
        return dato;
    };
    GlobalServices.prototype.agregarMensaje = function (msg) {
        this.flagsMessage.push(msg);
        console.log(this.flagsMessage);
    };
    GlobalServices.prototype.devolverNotificacines = function () {
        return this.flagsMessage;
    };
    GlobalServices.prototype.devolverCodigos = function () {
        return this.authHttp.get(this.urlServer + "/codigos").map(function (res) { return res.json(); });
    };
    return GlobalServices;
}());
GlobalServices = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common__["l" /* DatePipe */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common__["l" /* DatePipe */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__["AuthHttp"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__["AuthHttp"]) === "function" && _b || Object])
], GlobalServices);

var _a, _b;
//# sourceMappingURL=globals.js.map

/***/ }),

/***/ "../../../../../src/app/GeneralModule/styles/footer.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#menuFooter {\n  list-style: none;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin-left: 20px;\n  margin-bottom: 0;\n  padding: 0; }\n  #menuFooter a {\n    color: #93072E;\n    margin-right: 20px; }\n\n#franjaFooter {\n  width: 100%;\n  height: 30px;\n  background-color: #f7efde; }\n\nimg {\n  margin-right: 20px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/GeneralModule/styles/header.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#encabezado {\n  background-color: #701423;\n  color: white;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 20px 10px; }\n  #encabezado #pallets, #encabezado #infoCarro {\n    -webkit-box-flex: 2;\n        -ms-flex-positive: 2;\n            flex-grow: 2; }\n  #encabezado #lenguaje {\n    margin-right: 20px; }\n    #encabezado #lenguaje a {\n      color: white; }\n\n#menu {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  background-color: #907c59;\n  list-style: none;\n  margin: 0;\n  padding: 10px;\n  height: 40px; }\n  #menu li {\n    margin-right: 20px; }\n  #menu a {\n    color: white; }\n  #menu a:hover {\n    cursor: pointer;\n    cursor: hand;\n    text-decoration: underline; }\n\n#pallets {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around; }\n  #pallets pallet {\n    height: 85px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    border: 1px solid white;\n    padding: 7px; }\n\n#infoCarro {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n  #infoCarro mini-carro {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/GeneralModule/styles/lista.carro.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "producto {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  padding: 10px;\n  border: 1px solid #e3d8be;\n  background: #ffffff;\n  /* Old browsers */\n  /* IE9 SVG, needs conditional override of 'filter' to 'none' */\n  background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIzMiUiIHN0b3AtY29sb3I9IiNmZmZmZmYiIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjZjlmM2UzIiBzdG9wLW9wYWNpdHk9IjEiLz4KICA8L2xpbmVhckdyYWRpZW50PgogIDxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxIiBoZWlnaHQ9IjEiIGZpbGw9InVybCgjZ3JhZC11Y2dnLWdlbmVyYXRlZCkiIC8+Cjwvc3ZnPg==);\n  /* FF3.6+ */\n  /* Chrome,Safari4+ */\n  /* Chrome10+,Safari5.1+ */\n  /* Opera 11.10+ */\n  /* IE10+ */\n  background: linear-gradient(to right, #ffffff 32%, #f9f3e3 100%);\n  /* W3C */\n  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#f9f3e3',GradientType=1 );\n  /* IE6-8 */ }\n\nheader {\n  padding-top: 10px; }\n\nfooter {\n  margin-top: 20px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: end;\n      -ms-flex-align: end;\n          align-items: flex-end;\n  font-weight: bold; }\n  footer div {\n    margin-bottom: 10px; }\n    footer div span {\n      color: #93072E; }\n  footer #botones {\n    margin-top: 20px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/GeneralModule/styles/mini.carro.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#cantProd {\n  position: absolute;\n  top: 55px;\n  color: #93072E;\n  font-size: 18px;\n  font-weight: bold; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/GeneralModule/styles/requerimiento.exp.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "header {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n\n#reqExp {\n  padding: 10px; }\n\n.card {\n  border: solid 1px black;\n  margin-bottom: 20px; }\n\n.labelTitulo {\n  display: block;\n  margin-top: 20px; }\n  .labelTitulo label {\n    font-weight: bold; }\n\n.card-header {\n  background: #f2dcdb;\n  text-align: center;\n  font-weight: bold;\n  border-bottom: solid 1px black; }\n\n.form-check-label {\n  margin-right: 10px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/GeneralModule/templates/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<img src=\"/assets/img/imgFoot.jpg\" alt=\"Logo footer\">\r\n<ul id=\"menuFooter\">\r\n  <li><a>{{'site.privacidad' | translate}}</a></li>\r\n  <li><a>{{ 'site.terminos' | translate}}</a></li>\r\n</ul>\r\n<div id=\"franjaFooter\"></div>\r\n"

/***/ }),

/***/ "../../../../../src/app/GeneralModule/templates/header.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"encabezado\">\r\n  <div>\r\n    <img alt=\"logo juan valdez cafe\" src=\"/assets/img/logo2.png\" />\r\n  </div>\r\n  <div id=\"pallets\">\r\n    <ng-template [ngIf]=\"us.isLogin()\">\r\n      <pallet [capacidadPallets]=\"10\" [numContenedor]=\"20\"></pallet>\r\n      <pallet [capacidadPallets]=\"22\" [numContenedor]=\"40\"></pallet>\r\n    </ng-template>\r\n  </div>\r\n  <div id=\"infoCarro\">\r\n    <ng-template [ngIf]=\"us.isLogin()\">\r\n      <mini-carro></mini-carro>\r\n    </ng-template>\r\n  </div>\r\n  <div id=\"lenguaje\">\r\n    <a (click)=\"cambiarIdioma()\">{{ 'lenguaje' | translate}}</a>\r\n  </div>\r\n</div>\r\n<nav >\r\n  <ul id=\"menu\">\r\n    <ng-template [ngIf]=\"us.isLogin()\">\r\n      <li><a [routerLink]=\"['/']\">{{'header.todosProductos' | translate}}</a></li>\r\n      <!--<li><a [routerLink]=\"['/']\">Todos los productos</a></li>-->\r\n      <!--<li><a>Admin</a></li>-->\r\n      <li><a (click)=\"logout()\">{{'header.logout' | translate}}</a></li>\r\n      <!--<li><a (click)=\"logout()\">Finalizar sesion</a></li>-->\r\n\r\n    </ng-template>\r\n  </ul>\r\n</nav>\r\n"

/***/ }),

/***/ "../../../../../src/app/GeneralModule/templates/lista.carro.component.html":
/***/ (function(module, exports) {

module.exports = "<header>\r\n  <button class=\"btn\" (click)=\"cambiarTipoVista()\">{{ textoVista | translate }}</button>\r\n</header>\r\n<div *ngIf=\"tipoVista === 0; else vistaTabla\">\r\n  <div *ngFor=\"let p of prodCart\" style=\"margin-top: 20px;\">\r\n    <producto [producto]=\"p\" [view]=\"1\" (onUpdate)=\"anadirProducto($event)\" (onDeleted)=\"quitarProducto($event)\"></producto>\r\n  </div>\r\n</div>\r\n\r\n<ng-template #vistaTabla>\r\n  <table id=\"table-productos\" class=\"table table-bordered table-hover\" style=\"margin-top: 20px;\">\r\n    <thead>\r\n    <tr>\r\n      <th>Sku</th>\r\n\r\n      <!--<th>Descripcion</th>-->\r\n      <!--<th>Unidades por multiplo</th>-->\r\n      <!--<th>Cantidad</th>-->\r\n      <!--<th>Cantidad Cajas</th>-->\r\n      <!--<th>Cantidad Tendidos</th>-->\r\n      <!--<th>Cantidad Pallets</th>-->\r\n\r\n      <th>{{ 'listpro.description' | translate}}</th>\r\n      <th>{{ 'listpro.unidades' | translate}}</th>\r\n      <th>{{ 'listpro.cantidad' | translate}}</th>\r\n      <th>{{ 'listpro.cantCajas' | translate}}</th>\r\n      <th>{{ 'listpro.cantTendidos' | translate}}</th>\r\n      <th>{{ 'listpro.cantPallets' | translate}}</th>\r\n      <th></th>\r\n      <th></th>\r\n    </tr>\r\n    </thead>\r\n    <tbody>\r\n    <tr *ngFor=\"let prod of prodCart\">\r\n      <!--<producto [producto]=\"prod\" [view]=\"1\" (onSelect)=\"anadirProducto($event)\"></producto>-->\r\n      <td>{{ prod.sku | noZeros }}</td>\r\n      <td>{{ prod.nombre }}</td>\r\n      <td>{{ prod.multiplo }}</td>\r\n      <td><input class=\"col form-control\" type=\"number\" min=\"{{ prod.multiplo }}\" value=\"{{ prod.multiplo }}\" step=\"{{ prod.multiplo }}\" [(ngModel)]=\"prod.cantidad\"/></td>\r\n      <td>{{ (prod.cantCajas * (prod.cantidad / prod.multiplo)) | number:'1.0-2'}}</td>\r\n      <td>{{ (prod.cantTendidos * (prod.cantidad / prod.multiplo)) | number:'1.0-2' }}</td>\r\n      <td>{{ (prod.cantPallets * (prod.cantidad / prod.multiplo)) | number:'1.0-2' }}</td>\r\n      <!--<td><button class=\"btn\" (click)=\"anadirProducto(prod)\">Actualizar</button></td>-->\r\n      <!--<td><button class=\"btn\" (click)=\"quitarProducto(prod)\">QuitarProducto</button></td>-->\r\n      <td><button class=\"btn\" (click)=\"anadirProducto(prod)\">{{ 'btn.actualizar' | translate }}</button></td>\r\n      <td><button class=\"btn\" (click)=\"quitarProducto(prod)\">{{ 'btn.quitarPro' | translate }}</button></td>\r\n    </tr>\r\n    </tbody>\r\n  </table>\r\n</ng-template>\r\n\r\n<footer>\r\n  <div>\r\n    <span>{{ 'car.cajas' | translate }}:</span> {{ cs.cantTotalCajas() | number:'1.0-2' }}\r\n  </div>\r\n  <div>\r\n    <span>{{ 'car.tendidos' | translate }}:</span> {{ cs.cantTotalTendidos() | number:'1.0-2' }}\r\n  </div>\r\n  <div>\r\n    <span>{{ 'car.pallets' | translate }}:</span> {{ cs.cantTotalPallets() | number:'1.0-2' }}\r\n  </div>\r\n  <div>\r\n    <span>{{ 'car.totalValor' | translate  }}:</span> {{ cs.cantTotal() | number:'1.0-2' }}\r\n  </div>\r\n  <div id=\"botones\">\r\n    <!--<button class=\"btn\" (click)=\"vaciarCarrito()\">Vaciar Pedido</button>-->\r\n    <!--<button class=\"btn\" [routerLink]=\"['/']\">Continuar ingresando productos</button>-->\r\n    <!--<button class=\"btn\" (click)=\"aprobarPedido()\">Continuar</button>-->\r\n\r\n    <button class=\"btn\" (click)=\"vaciarCarrito()\">{{ 'btn.vaciarPedido' | translate }}</button>\r\n    <button class=\"btn\" [routerLink]=\"['/']\">{{ 'btn.listaProductos'  | translate }}</button>\r\n    <button class=\"btn\" (click)=\"aprobarPedido()\">{{ 'btn.continuar' | translate }}</button>\r\n  </div>\r\n</footer>\r\n\r\n<ng-template #confirmarCompra>\r\n  <div class=\"modal-header\">\r\n    <h4>{{ \"btn.confirmar\" | translate }}</h4>\r\n  </div>\r\n  <div class=\"modal-body\" *ngIf=\"prodSelect\">\r\n    <p *ngIf=\"errorMsg; else msgConf\">{{ errorMsg }}</p>\r\n\r\n    <ng-template #msgConf>\r\n      <!--<p>Confirmar que desea agregar {{ prodSelect.cantidad }} und de {{prodSelect.nombre}} a su pedido</p>-->\r\n      <p>{{ 'msgConf.msg1' | translate }} {{ prodSelect.cantidad }} {{ 'msgConf.msg2' | translate }} {{prodSelect.nombre}} {{ 'msgConf.msg3' | translate }}</p>\r\n    </ng-template>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <button class=\"btn btn-light\" (click)=\"modalRef.hide()\">{{ 'btn.cerrar' | translate }}</button>\r\n    <button class=\"btn btn-primary\" (click)=\"actualizarProducto()\" *ngIf=\"errorMsg == ''\">{{ 'btn.aceptar' | translate }}</button>\r\n  </div>\r\n</ng-template>\r\n"

/***/ }),

/***/ "../../../../../src/app/GeneralModule/templates/mini.carro.component.html":
/***/ (function(module, exports) {

module.exports = "<img class=\"img-fluid\" src=\"/assets/img/bolsa.png\" />\r\n<div id=\"cantProd\">\r\n  <span *ngIf=\"(prodCart != null)\">{{ (prodCart$ | async).length }}</span>\r\n  <span *ngIf=\"(prodCart == null)\">0</span>\r\n</div>\r\n<!--<p *ngIf=\"(prodCart == null); else conProductos\" class=\"infoCarro\">Su carro esta vacio</p>-->\r\n<p *ngIf=\"(prodCart == null); else conProductos\" class=\"infoCarro\">{{ 'car.empty' | translate }}</p>\r\n<ng-template #conProductos>\r\n  <!--<p class=\"infoCarro\">Productos en su <a [routerLink]=\"['carrito']\">pedido</a></p>-->\r\n  <p class=\"infoCarro\">{{'car.info1' | translate }} <a [routerLink]=\"['carrito']\">{{ 'car.info2' | translate }}</a></p>\r\n</ng-template>\r\n\r\n"

/***/ }),

/***/ "../../../../../src/app/GeneralModule/templates/requerimiento.exp.component.html":
/***/ (function(module, exports) {

module.exports = "<header>\r\n  <img>\r\n  <!--<h2>PROMOTORA DE CAFE DE COLOMBIA S.A. FORMATO SOLICITUD REQUERIMIENTO DE EXPORTACION</h2>-->\r\n  <h2>PROMOTORA DE CAFE DE COLOMBIA S.A. {{'req.formatoExport' | translate}}</h2>\r\n  <div></div>\r\n</header>\r\n<div id=\"reqExp\">\r\n\r\n\r\n  <div class=\"card\">\r\n    <div class=\"card-header\">\r\n      <!--1. Datos de negociación-->\r\n\r\n      {{ 'req.title1' | translate }}\r\n    </div>\r\n    <div class=\"card-body\">\r\n      <div class=\"row\">\r\n        <div class=\"col\">\r\n          <div style=\"display: block\">\r\n            <!--<label>Termino Incoterms 2010</label>-->\r\n            <label>{{ 'req.incoterm' | translate }}</label>\r\n          </div>\r\n\r\n          <div *ngFor=\"let c of codInco\" style=\"display: inline\">\r\n            <label class=\"form-check-label\">\r\n            <input type=\"radio\" class=\"form-check-input\" popoverTitle=\"{{ c.codigo }}\" popover=\"{{ c.codigo | translate }}\" triggers=\"mouseenter:mouseleave\" [(ngModel)]=\"req.dtNegociacion.incoterm\" value=\"{{ c.codigo }}\" name=\"incoterm\" (ngModelChange)=\"cambioTermino($event)\"  />\r\n              {{ c.codigo }}\r\n            </label>\r\n          </div>\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.incoterm\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n        </div>\r\n        <div class=\"col\">\r\n          <!--<label>Flete</label>-->\r\n          <label>{{'req.flete' | translate }}</label>\r\n          <input readonly [(ngModel)]=\"req.dtNegociacion.flete.nombre\" class=\"form-control\" type=\"text\" name=\"flete\" />\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div class=\"col\">\r\n          <div class=\"labelTitulo\">\r\n            <!--<label>Emisión de BL</label>-->\r\n            <label>{{ 'req.emisionBL' | translate }}</label>\r\n          </div>\r\n          <!--<p class=\"text-info\">Para embarques Aéreos, la Guía Aérea o Air Way Bill será entregada con la mercadería</p>-->\r\n          <p class=\"text-info\">{{ 'req.detEmisionBL' | translate }}</p>\r\n\r\n          <label class=\"form-check-label\">\r\n            <input class=\"form-check-input\" type=\"radio\" [(ngModel)]=\"req.dtNegociacion.emisionBL\" value=\"Origen\" name=\"emisionBL\" />\r\n            <!--Origen-->\r\n            {{ 'req.origen' | translate }}\r\n          </label>\r\n          <label  class=\"form-check-label\">\r\n            <input class=\"form-check-input\" type=\"radio\" [(ngModel)]=\"req.dtNegociacion.emisionBL\" value=\"Destino\" name=\"emisionBL\"  />\r\n            <!--Destino-->\r\n            {{ 'req.destino' | translate }}\r\n          </label>\r\n\r\n          <!--<h6 class=\"text-info\" style=\"margin-top: 20px;\">*Para Términos DDP o DAP</h6>-->\r\n          <h6 class=\"text-info\" style=\"margin-top: 20px;\">{{'req.detEmisionBL2' | translate }}</h6>\r\n          <alert type=\"warning\" *ngIf=\"errors.emisionBL\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <!--<label>Dirección de entrega de la M/cia</label>-->\r\n          <label>{{'req.detEmisionBL3' | translate }}</label>\r\n          <input class=\"form-control\" name=\"direccionMercancia\" [(ngModel)]=\"req.dtNegociacion.direccionMercancia\" type=\"text\" [attr.readonly]=\"evaluarEmisionBL()\" />\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.direccionMercancia\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <!--<label>Codigo ZIP</label>-->\r\n          <label>{{ 'req.codZIP' | translate }}</label>\r\n          <input class=\"form-control\" name=\"codigoZip\" [(ngModel)]=\"req.dtNegociacion.codigoZIP\" type=\"text\" [attr.readonly]=\"evaluarEmisionBL()\" />\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.codigoZIP\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <!--<label>Contacto</label>-->\r\n          <label>{{ 'req.contacto' | translate }}</label>\r\n          <input class=\"form-control\" name=\"contacto\" [(ngModel)]=\"req.dtNegociacion.contacto\" type=\"text\" [attr.readonly]=\"evaluarEmisionBL()\" />\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.contacto\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <!--<label>Telefono - email</label>-->\r\n          <label>{{ 'req.telefono' | translate }}</label>\r\n          <input class=\"form-control\" name=\"telem\" [(ngModel)]=\"req.dtNegociacion.telem\" type=\"text\" [attr.readonly]=\"evaluarEmisionBL()\"  />\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.telem\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n        </div>\r\n        <div class=\"col\"></div>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"card\">\r\n    <div class=\"card-header\">\r\n      <!--2. Datos de despacho-->\r\n      {{ 'req.title2' | translate }}\r\n    </div>\r\n    <div class=\"card-body\">\r\n      <div class=\"row\">\r\n        <div class=\"col\">\r\n          <div class=\"form-group\">\r\n            <!--<label>Modalidad de embarque</label>-->\r\n            <label>{{ 'req.modalidadEmb' | translate }}</label>\r\n            <select class=\"form-control\" name=\"modEmbarque\" [(ngModel)]=\"req.dtDespacho.modalidadEmbarque\" (change)=\"onModalidadEnbarqueChange($event)\" >\r\n              <option value=\"{{ c.codigo }}\" *ngFor=\"let c of modalidadEmbarque\">{{ c.descripcion | translate}}</option>\r\n            </select>\r\n          </div>\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.modalidadEmbarque\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <div class=\"form-group\">\r\n            <!--<label>Fecha Posible de Despacho</label>-->\r\n            <label>{{ 'req.fechaDesp' | translate }}</label>\r\n            <input class=\"form-control\" name=\"fechaDespacho\" type=\"date\" [(ngModel)]=\"req.dtDespacho.fechaPosibleDespacho\" />\r\n          </div>\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.fechaPosibleDespacho\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <div class=\"form-group\" *ngIf=\"contenedorHabilitado\">\r\n            <div style=\"display: block\">\r\n              <!--<label>Tipo de contenedor</label>-->\r\n              <label>{{ 'req.tipcontenedor' | translate }}</label>\r\n            </div>\r\n            <select class=\"form-control\"  [(ngModel)]=\"req.dtDespacho.tipoContenedor\" >\r\n              <option value=\"{{ c.codigo }}\" *ngFor=\"let c of tipoContenedor\">{{ c.descripcion | translate }}</option>\r\n            </select>\r\n\r\n            <alert type=\"warning\" *ngIf=\"errors.tipoContenedor\">\r\n              {{'req.diligenciarCampo' | translate}}\r\n            </alert>\r\n\r\n          </div>\r\n\r\n          <div class=\"form-group\" *ngIf=\"contenedorHabilitado\">\r\n            <!--<label>Cantidad de contenedores</label>-->\r\n            <label>{{ 'req.contenedor' | translate }}</label>\r\n            <input class=\"form-control\" type=\"text\" [(ngModel)]=\"req.dtDespacho.valorContenedor\" />\r\n          </div>\r\n          <alert type=\"warning\" *ngIf=\"errors.valorContenedor\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n        </div>\r\n        <div class=\"col\">\r\n          <div class=\"form-group\">\r\n            <!--<label>Puerto de Salida</label>-->\r\n            <label>{{ 'req.puertoSalida' | translate }}</label>\r\n            <select class=\"form-control\"  [attr.readonly]=\"puertoSalidaActivado()\" [attr.required]=\"puertoSalidaActivado()\" [(ngModel)]=\"req.dtDespacho.puertoSalida\">\r\n              <option value=\"{{ c.codigo }}\" *ngFor=\"let c of pSalidas\">{{ c.descripcion }}</option>\r\n            </select>\r\n          </div>\r\n          <alert type=\"warning\" *ngIf=\"errors.puertoSalida\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n          <div class=\"form-group\">\r\n            <!--<label>Puerto de Llegada</label>-->\r\n            <label>{{ 'req.puertoLlegada' | translate }}</label>\r\n            <!--<input class=\"form-control\" type=\"text\" [(ngModel)]=\"req.dtDespacho.puertoLlegada\"  [attr.readonly]=\"puertoLlegadaActivado()\" [attr.required]=\"puertoLlegadaActivado()\" />-->\r\n            <select class=\"form-control\"  [attr.readonly]=\"puertoLlegadaActivado()\" [attr.required]=\"puertoLlegadaActivado()\" [(ngModel)]=\"req.dtDespacho.puertoLlegada\">\r\n              <option value=\"{{ c.codigo }}\" *ngFor=\"let c of pLlegadas\">{{ c.descripcion }}</option>\r\n            </select>\r\n          </div>\r\n          <alert type=\"warning\" *ngIf=\"errors.puertoLlegada\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"card\">\r\n    <div class=\"card-header\">\r\n      <!--3. Datos de agenciamiento-->\r\n      {{ 'req.title3' | translate }}\r\n    </div>\r\n    <div class=\"card-body\">\r\n      <div class=\"row\">\r\n        <div class=\"col\">\r\n          <label>1. Service Contract*</label>\r\n          <input class=\"form-control\" type=\"text\" [(ngModel)]=\"req.serviceContract\" [attr.readonly]=\"evaluarAgenciaCarga()\" [attr.required]=\"evaluarAgenciaCarga()\"/>\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.servicesContract\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <div class=\"labelTitulo\">\r\n            <!--<label>2. Agencia de Carga (En Colombia)*</label>-->\r\n            <label>{{ 'req.agenciaCarga' | translate }}</label>\r\n          </div>\r\n\r\n          <div *ngIf=\"evaluarAgenciaCarga() == null\">\r\n            <div class=\"form-group\">\r\n              <!--<label>Razon Social</label>-->\r\n              <label>{{'req.razonSocial' | translate}}</label>\r\n              <input class=\"form-control\" type=\"text\" [(ngModel)]=\"req.agenciaCarga.razonSocial\" [attr.readonly]=\"evaluarAgenciaCarga()\" [attr.required]=\"evaluarAgenciaCarga()\"/>\r\n            </div>\r\n\r\n            <alert type=\"warning\" *ngIf=\"errors.acrazonSocial\">\r\n              {{'req.diligenciarCampo' | translate}}\r\n            </alert>\r\n\r\n            <div class=\"form-group\">\r\n              <!--<label>Contacto</label>-->\r\n              <label>{{'req.contacto' | translate}}</label>\r\n              <input class=\"form-control\" type=\"text\"  [(ngModel)]=\"req.agenciaCarga.contacto\" [attr.readonly]=\"evaluarAgenciaCarga()\" [attr.required]=\"evaluarAgenciaCarga()\"/>\r\n            </div>\r\n\r\n            <alert type=\"warning\" *ngIf=\"errors.accontacto\">\r\n              {{'req.diligenciarCampo' | translate}}\r\n            </alert>\r\n\r\n            <div class=\"form-group\">\r\n              <!--<label>Telefono - email</label>-->\r\n              <label>{{ 'req.telefono' | translate }}</label>\r\n              <input class=\"form-control\" type=\"text\" [(ngModel)]=\"req.agenciaCarga.telefono\" [attr.readonly]=\"evaluarAgenciaCarga()\" [attr.required]=\"evaluarAgenciaCarga()\"/>\r\n            </div>\r\n\r\n            <alert type=\"warning\" *ngIf=\"errors.actelefono\">\r\n              {{'req.diligenciarCampo' | translate}}\r\n            </alert>\r\n          </div>\r\n\r\n\r\n          <div class=\"labelTitulo\">\r\n            <!--<label>3. Consignatario</label>-->\r\n            <label>{{'req.consignatario' | translate}}</label>\r\n          </div>\r\n\r\n          <div class=\"form-group\">\r\n            <!--<label>Razon Social</label>-->\r\n            <label>{{'req.razonSocial' | translate}}</label>\r\n            <input class=\"form-control\" type=\"text\"  [(ngModel)]=\"req.consignatario.razonSocial\"/>\r\n          </div>\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.corazonSocial\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <div class=\"form-group\">\r\n            <label>Nit/ID</label>\r\n            <input class=\"form-control\" type=\"text\" [(ngModel)]=\"req.consignatario.nit\"/>\r\n          </div>\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.conit\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <div class=\"form-group\">\r\n            <!--<label>Direccion del Cliente</label>-->\r\n            <label>{{'req.direccionCli' | translate}}</label>\r\n            <input class=\"form-control\" type=\"text\" [(ngModel)]=\"req.consignatario.direccion\"/>\r\n          </div>\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.codireccion\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <div class=\"form-group\">\r\n            <!--<label>Telefono - email</label>-->\r\n            <label>{{'req.telefono' | translate}}</label>\r\n            <input class=\"form-control\" type=\"text\"  [(ngModel)]=\"req.consignatario.telefono\"/>\r\n          </div>\r\n          <alert type=\"warning\" *ngIf=\"errors.cotelefono\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <div class=\"form-group\">\r\n            <!--<label>Ciudad y Pais</label>-->\r\n            <label>{{'req.ciudad' | translate}}</label>\r\n            <input class=\"form-control\" type=\"text\"  [(ngModel)]=\"req.consignatario.ciudad\"/>\r\n          </div>\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.cociudad\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <div class=\"form-group\">\r\n            <!--<label>Direccion envio de Documentacion</label>-->\r\n            <label>{{'req.direccionEnvio' | translate}}</label>\r\n            <input class=\"form-control\" type=\"text\"  [(ngModel)]=\"req.consignatario.direccionEnvio\"/>\r\n          </div>\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.codireccionEnvio\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <div class=\"form-group\">\r\n            <label class=\"form-check-label\">\r\n              <input type=\"checkbox\" class=\"form-check-input\" [(ngModel)]=\"mismoConsigNoti\" (ngModelChange)=\"ajustarInfoConsigNoti($event)\"/>\r\n              <!--Los datos del Consignatario son los mismos del notificado-->\r\n              {{'req.datosConsig' | translate}}\r\n            </label>\r\n          </div>\r\n\r\n        </div>\r\n\r\n        <div class=\"col\">\r\n          <div class=\"labelTitulo\" style=\"margin-top: 0\">\r\n            <!--<label>4. Notificado</label>-->\r\n            <label>{{'req.notificado' | translate}}</label>\r\n          </div>\r\n\r\n          <div class=\"form-group\">\r\n            <!--<label>Razon Social</label>-->\r\n            <label>{{'req.razonSocial' | translate}}</label>\r\n            <input class=\"form-control\" type=\"text\" [(ngModel)]=\"req.notificado.razonSocial\"/>\r\n          </div>\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.norazonSocial\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <div class=\"form-group\">\r\n            <label>Nit/ID</label>\r\n            <input class=\"form-control\"  type=\"text\" [(ngModel)]=\"req.notificado.nit\"/>\r\n          </div>\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.nonit\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <div class=\"form-group\">\r\n            <!--<label>Direccion del Cliente</label>-->\r\n            <label>{{'req.direccionCli' | translate}}</label>\r\n            <input class=\"form-control\" type=\"text\" [(ngModel)]=\"req.notificado.direccion\"/>\r\n          </div>\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.nodireccion\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <div class=\"form-group\">\r\n            <!--<label>Telefono - email</label>-->\r\n            <label>{{'req.telefono' | translate }}</label>\r\n            <input class=\"form-control\" type=\"text\" [(ngModel)]=\"req.notificado.telefono\"/>\r\n          </div>\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.notelefono\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n          <div class=\"form-group\">\r\n            <!--<label>Ciudad y Pais</label>-->\r\n            <label>{{'req.ciudad' | translate}}</label>\r\n            <input class=\"form-control\" type=\"text\" [(ngModel)]=\"req.notificado.ciudad\"/>\r\n          </div>\r\n\r\n          <alert type=\"warning\" *ngIf=\"errors.nociudad\">\r\n            {{'req.diligenciarCampo' | translate}}\r\n          </alert>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"card\">\r\n    <div class=\"card-header\">\r\n      <!--4. Documentos adicionales para nacionalizacion en destino-->\r\n      {{ 'req.title4' | translate }}\r\n    </div>\r\n\r\n    <div class=\"card-body\">\r\n      <div class=\"row\">\r\n        <div class=\"col\">\r\n\r\n          <div class=\"form-group\" *ngFor=\"let doc of tipoDocumentos\">\r\n            <label class=\"form-check-label\">\r\n              <input class=\"form-check-input\" type=\"checkbox\" value=\"{{ doc.value }}\" [(ngModel)]=\"doc.checked\" name=\"tipoDocumentos\" (ngModelChange)=\"ajusteDocumentos($event)\" /> {{ doc.name | translate }}\r\n            </label>\r\n          </div>\r\n\r\n          <div *ngIf=\"validarOtrosReq()\">\r\n            <!--<label>Cuales</label>-->\r\n            <label>{{'req.cuales' | translate}}</label>\r\n            <textarea [(ngModel)]=\"req.otrosRequerimientos\" class=\"form-control col-sm-6\" [attr.required]=\"validarOtrosReq()\">\r\n\r\n            </textarea>\r\n          </div>\r\n\r\n\r\n\r\n          <div class=\"labelTitulo\">\r\n            <!--<label>La factura lleva observaciones</label>-->\r\n            <label>{{'req.obsFacturas' | translate}}</label>\r\n          </div>\r\n\r\n          <div class=\"form-group\">\r\n\r\n            <label class=\"form-check-label\">\r\n              <input class=\"form-check-input\" type=\"radio\" [value]=\"true\" [(ngModel)]=\"req.llevaObservaciones\" />\r\n              {{'req.si' | translate}}\r\n            </label>\r\n\r\n            <label class=\"form-check-label\">\r\n              <input class=\"form-check-input\" type=\"radio\" [value]=\"false\" [(ngModel)]=\"req.llevaObservaciones\" />\r\n              {{'req.no' | translate}}\r\n            </label>\r\n\r\n          </div>\r\n\r\n          <div class=\"form-group\" *ngIf=\"req.llevaObservaciones\">\r\n            <!--<label>Cuales</label>-->\r\n            <label>{{'req.cuales' | translate}}</label>\r\n            <textarea [(ngModel)]=\"req.observaciones\" class=\"form-control col-sm-6\" [attr.required]=\"validarObsFactura()\">\r\n\r\n            </textarea>\r\n          </div>\r\n          <!--<p *ngIf=\"req.llevaObservaciones\">Maximo 100 caracteres</p>-->\r\n          <p *ngIf=\"req.llevaObservaciones\">{{ 'req.llevaObs' | translate }}</p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <div class=\"card\">\r\n    <div class=\"card-header\">\r\n      <!--5. Marcacion Especial-->\r\n      {{ 'req.title5' | translate }}\r\n    </div>\r\n\r\n    <div class=\"card-body\">\r\n      <div class=\"row\">\r\n        <div class=\"col\">\r\n          <label class=\"form-check-label\">\r\n            <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"req.marcacionEspecial\"/>{{'req.marcacion' | translate }}\r\n          </label>\r\n        </div>\r\n      </div>\r\n      <div class=\"row\" *ngIf=\"req.marcacionEspecial\">\r\n        <div class=\"col\">\r\n          <table class=\"table table-bordered\">\r\n            <thead>\r\n              <tr>\r\n                <!--<th rowspan=\"2\" style=\"vertical-align: middle\" class=\"text-center\">NOMBRE DEL PRODUCTO</th>-->\r\n                <!--<th colspan=\"3\" class=\"text-center\">MARCAR EN:</th>-->\r\n                <!--<th rowspan=\"2\" style=\"vertical-align: middle\" class=\"text-center\">OBSERVACIONES</th>-->\r\n                <th rowspan=\"2\" style=\"vertical-align: middle\" class=\"text-center\">{{'req.marcNombreProd' | translate}}</th>\r\n                <th colspan=\"3\" class=\"text-center\">{{'req.marcMarcar' | translate}}</th>\r\n                <th rowspan=\"2\" style=\"vertical-align: middle\" class=\"text-center\">{{'req.marcObs' | translate}}</th>\r\n              </tr>\r\n            <tr>\r\n              <!--<th class=\"text-center\">Producto</th>-->\r\n              <!--<th class=\"text-center\">Caja Master</th>-->\r\n              <!--<th class=\"text-center\">Pallet</th>-->\r\n              <th class=\"text-center\">{{'req.producto' | translate}}</th>\r\n              <th class=\"text-center\">{{'req.cajaMaster' | translate}}</th>\r\n              <th class=\"text-center\">{{'req.pallet' | translate}}</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr style=\"background-color: #93072E; color: #ffffff; font-weight: bold\">\r\n                <td>{{'req.productos' | translate }}</td>\r\n                <td class=\"text-center\"><input type=\"checkbox\" (change)=\"cambioEnReqProducto($event)\" /></td>\r\n                <td class=\"text-center\"><input type=\"checkbox\" (change)=\"cambioEnReqCajaMaster($event)\" /></td>\r\n                <td class=\"text-center\"><input type=\"checkbox\" (change)=\"cambioEnReqPallets($event)\" /></td>\r\n                <td></td>\r\n              </tr>\r\n              <tr *ngFor=\"let prod of reqxProd\">\r\n                <td style=\"vertical-align: middle\">{{prod.nombre}}</td>\r\n                <td class=\"text-center\"><input type=\"checkbox\"  [(ngModel)]=\"prod.prod\" /></td>\r\n                <td class=\"text-center\"><input type=\"checkbox\" [(ngModel)]=\"prod.cajaMaster\" /></td>\r\n                <td class=\"text-center\"><input type=\"checkbox\" [(ngModel)]=\"prod.pallet\" /></td>\r\n                <td class=\"text-center\"><input type=\"text\" class=\"form-control\"  [(ngModel)]=\"prod.observaciones\" /></td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n\r\n  <button class=\"btn\" (click)=\"finalizarOrden()\">{{'req.btnFinalizar' | translate}}</button>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/ProductoModule/components/lista.producto.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_producto_model__ = __webpack_require__("../../../../../src/app/ProductoModule/models/producto.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_producto_services__ = __webpack_require__("../../../../../src/app/ProductoModule/services/producto.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__GeneralModule_services_carro_services__ = __webpack_require__("../../../../../src/app/GeneralModule/services/carro.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__UsuarioModule_services_usuario_services__ = __webpack_require__("../../../../../src/app/UsuarioModule/services/usuario.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__GeneralModule_services_globals__ = __webpack_require__("../../../../../src/app/GeneralModule/services/globals.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_translate__ = __webpack_require__("../../../../ng2-translate/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListaProductoComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ListaProductoComponent = (function () {
    function ListaProductoComponent(ps, modalService, carroService, us, GS, translate) {
        this.ps = ps;
        this.modalService = modalService;
        this.carroService = carroService;
        this.us = us;
        this.GS = GS;
        this.translate = translate;
        this.productos = [];
        this.prodFiltrados = [];
        this.prodSelect = null;
        this.tipoVista = 0;
        this.textoVista = "submenu.lista";
        this.filtroNombre = "";
        this.notificaciones = [];
        this.errorMsg = "";
    }
    ListaProductoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.productos = [];
        var datos = this.ps.getProductos().subscribe(function (response) {
            if (response != null) {
                for (var _i = 0, _a = response.productosXCliente; _i < _a.length; _i++) {
                    var d = _a[_i];
                    var p = __WEBPACK_IMPORTED_MODULE_1__models_producto_model__["a" /* Producto */].CrearProducto(d);
                    _this.productos.push(p);
                }
            }
            _this.prodFiltrados = _this.productos;
        }, function (error) {
            //this.us.irALogin();
        });
        // this.productos = PRODUCTOS;
        // this.prodFiltrados = this.productos;
        this.notificaciones = this.GS.devolverNotificacines();
        this.GS.flagsMessage = [];
    };
    ListaProductoComponent.prototype.anadirProducto = function (prod) {
        if (prod != null) {
            this.prodSelect = prod;
            this.abrirModal();
        }
    };
    ListaProductoComponent.prototype.productoSeleccionado = function (prod) {
        if (prod != null) {
            this.prodSelect = prod;
            this.abrirModal();
        }
    };
    ListaProductoComponent.prototype.adicionarAlCarrito = function () {
        this.carroService.almacenarProducto(this.prodSelect);
        // this.prodSelect.cantidad = this.prodSelect.multiplo;
        this.prodSelect = null;
        // falta agregar el producto al carrito
        this.modalRef.hide();
    };
    ListaProductoComponent.prototype.abrirModal = function () {
        var _this = this;
        if (this.prodSelect.cantidad >= this.prodSelect.multiplo && (this.prodSelect.cantidad % this.prodSelect.multiplo) == 0) {
            this.errorMsg = '';
        }
        else {
            this.translate.get('prod.errorMultiplo').subscribe(function (val) {
                _this.errorMsg = val + _this.prodSelect.multiplo;
            });
        }
        this.modalRef = this.modalService.show(this.templateModal);
    };
    ListaProductoComponent.prototype.filtrarXCat = function (cat) {
        if (cat !== "") {
            this.prodFiltrados = this.productos.filter(function (prod) {
                if (cat === "CF") {
                    if (prod.sector === "CF")
                        return true;
                    else
                        return false;
                }
                else {
                    if (prod.sector !== "CF")
                        return true;
                    else
                        return false;
                }
            });
        }
        else
            this.prodFiltrados = this.productos;
    };
    ListaProductoComponent.prototype.cambiarTipoVista = function () {
        if (this.tipoVista == 0) {
            this.tipoVista = 1;
            this.textoVista = "submenu.grafica";
            // this.translate.get('submenu.grafica').subscribe(val =>{
            //   this.textoVista = val;
            // })
        }
        else if (this.tipoVista == 1) {
            this.tipoVista = 0;
            this.textoVista = "submenu.lista";
            // this.translate.get('submenu.lista').subscribe(val =>{
            //   this.textoVista = val;
            // })
        }
    };
    ListaProductoComponent.prototype.filtrarPorNombre = function (filtro) {
        if (filtro !== "") {
            this.prodFiltrados = this.productos.filter(function (prod) {
                var nom = prod.nombre.toLowerCase();
                return nom.includes(filtro.toLowerCase());
                // return prod.nombre.includes(filtro);
            });
        }
        else {
            this.prodFiltrados = this.productos;
        }
    };
    return ListaProductoComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('confirmarCompra'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"]) === "function" && _a || Object)
], ListaProductoComponent.prototype, "templateModal", void 0);
ListaProductoComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "lista-productos",
        template: __webpack_require__("../../../../../src/app/ProductoModule/templates/lista.producto.component.html"),
        styles: [__webpack_require__("../../../../../src/app/ProductoModule/styles/lista.producto.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_producto_services__["a" /* ProductoServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_producto_services__["a" /* ProductoServices */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap__["c" /* BsModalService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap__["c" /* BsModalService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__GeneralModule_services_carro_services__["a" /* CarroServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__GeneralModule_services_carro_services__["a" /* CarroServices */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__UsuarioModule_services_usuario_services__["a" /* UsuarioServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__UsuarioModule_services_usuario_services__["a" /* UsuarioServices */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6__GeneralModule_services_globals__["a" /* GlobalServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__GeneralModule_services_globals__["a" /* GlobalServices */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_7_ng2_translate__["b" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7_ng2_translate__["b" /* TranslateService */]) === "function" && _g || Object])
], ListaProductoComponent);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=lista.producto.component.js.map

/***/ }),

/***/ "../../../../../src/app/ProductoModule/components/pallet.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__GeneralModule_services_carro_services__ = __webpack_require__("../../../../../src/app/GeneralModule/services/carro.services.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PalletComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PalletComponent = (function () {
    function PalletComponent(cs) {
        this.cs = cs;
        this.cantPallets = 0;
        this.estaLleno = false;
    }
    PalletComponent.prototype.ngOnInit = function () {
        this.porcLibre = 100 - ((this.cantPallets / this.capacidadPallets) * 100);
    };
    PalletComponent.prototype.devolverPorcentaje = function () {
        var pallets = this.cs.cantTotalPallets();
        var porcentaje = 100 - ((pallets / this.capacidadPallets) * 100);
        if (porcentaje < 0)
            porcentaje = 0;
        return porcentaje;
    };
    PalletComponent.prototype.devolvercantPallets = function () {
        var pallets = this.cs.cantTotalPallets();
        pallets = this.capacidadPallets - pallets;
        if (pallets < 0)
            pallets = 0;
        return pallets;
    };
    return PalletComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], PalletComponent.prototype, "numContenedor", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], PalletComponent.prototype, "capacidadPallets", void 0);
PalletComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'pallet',
        template: __webpack_require__("../../../../../src/app/ProductoModule/templates/pallet.component.html"),
        styles: [__webpack_require__("../../../../../src/app/ProductoModule/styles/pallet.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__GeneralModule_services_carro_services__["a" /* CarroServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__GeneralModule_services_carro_services__["a" /* CarroServices */]) === "function" && _a || Object])
], PalletComponent);

var _a;
//# sourceMappingURL=pallet.component.js.map

/***/ }),

/***/ "../../../../../src/app/ProductoModule/components/producto.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_producto_model__ = __webpack_require__("../../../../../src/app/ProductoModule/models/producto.model.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductoComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductoComponent = (function () {
    function ProductoComponent() {
        this.onSelect = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.onUpdate = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.onDeleted = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    ProductoComponent.prototype.productoSeleccionado = function () {
        this.onSelect.emit(this.producto);
    };
    ProductoComponent.prototype.actualizarProducto = function () {
        this.onUpdate.emit(this.producto);
    };
    ProductoComponent.prototype.borrarProducto = function () {
        this.onDeleted.emit(this.producto);
    };
    return ProductoComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_producto_model__["a" /* Producto */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models_producto_model__["a" /* Producto */]) === "function" && _a || Object)
], ProductoComponent.prototype, "producto", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], ProductoComponent.prototype, "view", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], ProductoComponent.prototype, "onSelect", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], ProductoComponent.prototype, "onUpdate", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], ProductoComponent.prototype, "onDeleted", void 0);
ProductoComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "producto",
        template: __webpack_require__("../../../../../src/app/ProductoModule/templates/producto.component.html"),
        styles: [__webpack_require__("../../../../../src/app/ProductoModule/styles/producto.component.scss")]
    })
], ProductoComponent);

var _a;
//# sourceMappingURL=producto.component.js.map

/***/ }),

/***/ "../../../../../src/app/ProductoModule/components/producto.info.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_producto_model__ = __webpack_require__("../../../../../src/app/ProductoModule/models/producto.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_producto_services__ = __webpack_require__("../../../../../src/app/ProductoModule/services/producto.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__GeneralModule_services_carro_services__ = __webpack_require__("../../../../../src/app/GeneralModule/services/carro.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_translate__ = __webpack_require__("../../../../ng2-translate/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductoInfoComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ProductoInfoComponent = (function () {
    function ProductoInfoComponent(route, ps, modalService, carroService, translate) {
        var _this = this;
        this.route = route;
        this.ps = ps;
        this.modalService = modalService;
        this.carroService = carroService;
        this.translate = translate;
        this.errorMsg = "";
        var id = null;
        this.route.params.forEach(function (params) {
            id = +params['id'];
        });
        this.ps.getProductos().subscribe(function (response) {
            for (var _i = 0, _a = response.productosXCliente; _i < _a.length; _i++) {
                var d = _a[_i];
                if (d.id == id) {
                    _this.producto = __WEBPACK_IMPORTED_MODULE_2__models_producto_model__["a" /* Producto */].CrearProducto(d);
                    console.log(d);
                    break;
                }
            }
        });
    }
    ProductoInfoComponent.prototype.adicionarAlCarrito = function () {
        this.carroService.almacenarProducto(this.producto);
        // falta agregar el producto al carrito
        this.modalRef.hide();
    };
    ProductoInfoComponent.prototype.anadirProducto = function () {
        var _this = this;
        if (this.producto.cantidad >= this.producto.multiplo && (this.producto.cantidad % this.producto.multiplo) == 0) {
            console.log(this.producto.cantidad % this.producto.multiplo);
            this.errorMsg = '';
        }
        else {
            this.translate.get('prod.errorMultiplo').subscribe(function (val) {
                _this.errorMsg = val + _this.producto.multiplo;
                _this.producto.cantidad = _this.producto.multiplo;
            });
        }
        this.modalRef = this.modalService.show(this.templateModal);
    };
    return ProductoInfoComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('confirmarCompra'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"]) === "function" && _a || Object)
], ProductoInfoComponent.prototype, "templateModal", void 0);
ProductoInfoComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "info-producto",
        template: __webpack_require__("../../../../../src/app/ProductoModule/templates/producto.info.component.html"),
        styles: [__webpack_require__("../../../../../src/app/ProductoModule/styles/producto.info.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_producto_services__["a" /* ProductoServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_producto_services__["a" /* ProductoServices */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap__["c" /* BsModalService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap__["c" /* BsModalService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__GeneralModule_services_carro_services__["a" /* CarroServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__GeneralModule_services_carro_services__["a" /* CarroServices */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6_ng2_translate__["b" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6_ng2_translate__["b" /* TranslateService */]) === "function" && _f || Object])
], ProductoInfoComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=producto.info.component.js.map

/***/ }),

/***/ "../../../../../src/app/ProductoModule/models/producto.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Producto; });
var Producto = (function () {
    function Producto(id, nombre, img, sku, valor, multiplo, cat, eq) {
        this.id = 0;
        this.img = "";
        this.nombre = "";
        this.sku = "";
        this.valor = 0;
        this.multiplo = 0;
        this.cantidad = 0;
        this.cat = 0;
        this.cantCajas = 0;
        this.cantTendidos = 0;
        this.cantPallets = 0;
        this.pesoNeto = 0;
        this.cantXCaja = 0;
        this.pesoBrutoxCaja = 0;
        this.totalCajasXPallet = 0;
        this.vidaUtil = 0;
        this.centro = "";
        this.sector = "";
        this.unidad = "";
        this.urlImg = "";
        this.id = id;
        this.nombre = nombre;
        this.img = img;
        this.sku = sku;
        this.valor = valor;
        this.multiplo = multiplo;
        this.cantidad = this.multiplo;
        // console.log(this.cantidad);
        this.cat = cat;
        this.pesoNeto = eq.peso_neto;
        this.cantXCaja = eq.cantidad_x_embalaje;
        this.pesoBrutoxCaja = eq.peso_bruto_embalaje;
        this.totalCajasXPallet = eq.total_cajas_x_pallet;
        this.vidaUtil = eq.tiempo_util;
        this.cantCajas = this.multiplo / this.cantXCaja;
        this.cantCajas.toFixed(2);
        this.cantTendidos = this.cantCajas / eq.cant_cajas_x_tendido;
        this.cantTendidos.toFixed(2);
        this.cantPallets = this.cantCajas / eq.total_cajas_x_pallet;
        this.cantPallets.toFixed(2);
    }
    Producto.prototype.devolverCantPallets = function () {
        var cajas = this.cantidad / this.cantCajas;
        var tendidos = cajas / this.cantTendidos;
        var pallets = cajas / this.cantPallets;
        return pallets;
    };
    Producto.CrearProducto = function (dato) {
        var p = new Producto(dato.id, dato.productosInventario[0].nombre, "", dato.productosInventario[0].sku, Number(dato.precioUsd), Number(dato.productosInventario[0].unidadMinimaDespachoXTendido), 1, dato.productosInventario[0].equivalencia);
        //p.cantCajas  = p.cantidad / Number(dato.productosInventario[0].equivalencia) ;
        p.centro = dato.productosInventario[0].centro;
        p.sector = dato.productosInventario[0].sector;
        p.unidad = dato.productosInventario[0].unidadDespacho.nombre;
        p.urlImg = "assets/materiales/" + p.sku + ".jpg";
        return p;
    };
    return Producto;
}());

//# sourceMappingURL=producto.model.js.map

/***/ }),

/***/ "../../../../../src/app/ProductoModule/pipes/eliminarCeros.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EliminarCerosPipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var EliminarCerosPipe = (function () {
    function EliminarCerosPipe() {
    }
    EliminarCerosPipe.prototype.transform = function (value) {
        return value.replace(/^0+/, '');
    };
    return EliminarCerosPipe;
}());
EliminarCerosPipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
        name: 'noZeros'
    })
], EliminarCerosPipe);

//# sourceMappingURL=eliminarCeros.pipe.js.map

/***/ }),

/***/ "../../../../../src/app/ProductoModule/producto.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__producto_routing_module__ = __webpack_require__("../../../../../src/app/ProductoModule/producto.routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_lista_producto_component__ = __webpack_require__("../../../../../src/app/ProductoModule/components/lista.producto.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_producto_component__ = __webpack_require__("../../../../../src/app/ProductoModule/components/producto.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_pallet_component__ = __webpack_require__("../../../../../src/app/ProductoModule/components/pallet.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_producto_info_component__ = __webpack_require__("../../../../../src/app/ProductoModule/components/producto.info.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ngx_soap__ = __webpack_require__("../../../../ngx-soap/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_translate__ = __webpack_require__("../../../../ng2-translate/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angular_font_awesome__ = __webpack_require__("../../../../angular-font-awesome/dist/angular-font-awesome.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pipes_eliminarCeros_pipe__ = __webpack_require__("../../../../../src/app/ProductoModule/pipes/eliminarCeros.pipe.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductoModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var ProductoModule = (function () {
    function ProductoModule() {
    }
    return ProductoModule;
}());
ProductoModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["k" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["HttpModule"],
            __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_2__producto_routing_module__["a" /* ProductoRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_7_ngx_bootstrap__["a" /* ModalModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_10_ngx_soap__["a" /* NgxSoapModule */],
            __WEBPACK_IMPORTED_MODULE_7_ngx_bootstrap__["b" /* AlertModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_11_ng2_translate__["a" /* TranslateModule */],
            __WEBPACK_IMPORTED_MODULE_12_angular_font_awesome__["a" /* AngularFontAwesomeModule */]
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__components_lista_producto_component__["a" /* ListaProductoComponent */],
            __WEBPACK_IMPORTED_MODULE_5__components_producto_component__["a" /* ProductoComponent */],
            __WEBPACK_IMPORTED_MODULE_9__components_producto_info_component__["a" /* ProductoInfoComponent */],
            __WEBPACK_IMPORTED_MODULE_8__components_pallet_component__["a" /* PalletComponent */],
            __WEBPACK_IMPORTED_MODULE_13__pipes_eliminarCeros_pipe__["a" /* EliminarCerosPipe */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_8__components_pallet_component__["a" /* PalletComponent */],
            __WEBPACK_IMPORTED_MODULE_5__components_producto_component__["a" /* ProductoComponent */],
            __WEBPACK_IMPORTED_MODULE_13__pipes_eliminarCeros_pipe__["a" /* EliminarCerosPipe */]
        ]
    })
], ProductoModule);

//# sourceMappingURL=producto.module.js.map

/***/ }),

/***/ "../../../../../src/app/ProductoModule/producto.routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_lista_producto_component__ = __webpack_require__("../../../../../src/app/ProductoModule/components/lista.producto.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_producto_info_component__ = __webpack_require__("../../../../../src/app/ProductoModule/components/producto.info.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__AuthModule_services_auth_guard_services__ = __webpack_require__("../../../../../src/app/AuthModule/services/auth-guard.services.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductoRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var productoRoutes = [
    { path: "", component: __WEBPACK_IMPORTED_MODULE_2__components_lista_producto_component__["a" /* ListaProductoComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_4__AuthModule_services_auth_guard_services__["a" /* AuthGuardServices */]] },
    { path: "producto/:id", component: __WEBPACK_IMPORTED_MODULE_3__components_producto_info_component__["a" /* ProductoInfoComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_4__AuthModule_services_auth_guard_services__["a" /* AuthGuardServices */]] }
];
var ProductoRoutingModule = (function () {
    function ProductoRoutingModule() {
    }
    return ProductoRoutingModule;
}());
ProductoRoutingModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */].forChild(productoRoutes)
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */]
        ]
    })
], ProductoRoutingModule);

//# sourceMappingURL=producto.routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/ProductoModule/services/producto.services.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular2_jwt__ = __webpack_require__("../../../../angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angular2_jwt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__GeneralModule_services_globals__ = __webpack_require__("../../../../../src/app/GeneralModule/services/globals.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductoServices; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductoServices = (function () {
    function ProductoServices(globals, authHttp) {
        this.globals = globals;
        this.authHttp = authHttp;
    }
    ProductoServices.prototype.getProductos = function () {
        // this.http.get(this.url).map(res=> res.json()).subscribe(response=>{
        //
        //   for(let d of response.productosXCliente){
        //     console.log(d);
        //   }
        // })
        var cliente = localStorage.getItem("cliente");
        var destinatario = localStorage.getItem("destinatario");
        var canal = localStorage.getItem('canal');
        return this.authHttp.post(this.globals.urlServer + "/materiales", { cliente: destinatario, canal: canal }).map(function (res) { return res.json(); });
    };
    return ProductoServices;
}());
ProductoServices = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__GeneralModule_services_globals__["a" /* GlobalServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__GeneralModule_services_globals__["a" /* GlobalServices */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_angular2_jwt__["AuthHttp"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_angular2_jwt__["AuthHttp"]) === "function" && _b || Object])
], ProductoServices);

var _a, _b;
//# sourceMappingURL=producto.services.js.map

/***/ }),

/***/ "../../../../../src/app/ProductoModule/styles/lista.producto.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#menuSec {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  background-color: #F9F3E5;\n  padding: 5px;\n  margin: 0 -10px; }\n  #menuSec ul {\n    list-style: none;\n    margin: 0;\n    padding: 0;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    height: 40px;\n    -webkit-box-flex: 1;\n        -ms-flex-positive: 1;\n            flex-grow: 1; }\n    #menuSec ul li {\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      margin-left: 20px;\n      color: #93072E; }\n  #menuSec .input-group {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    width: 250px;\n    margin-right: 20px; }\n\n#listaProductos {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin: 0;\n  padding: 0;\n  margin-top: 20px;\n  list-style: none;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap; }\n  #listaProductos li {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    margin-right: 20px;\n    margin-bottom: 10px;\n    width: 250px; }\n\n#table-productos {\n  margin-top: 20px; }\n  #table-productos td, #table-productos th {\n    text-align: center; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/ProductoModule/styles/pallet.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/ProductoModule/styles/producto.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".card-producto {\n  height: 100%;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n  .card-producto img {\n    height: 181px;\n    width: auto; }\n  .card-producto .card-body {\n    background-color: #F9F3E5; }\n    .card-producto .card-body h5 {\n      height: 50px; }\n    .card-producto .card-body p {\n      margin: 0; }\n      .card-producto .card-body p span {\n        color: #93072E;\n        font-weight: bold; }\n    .card-producto .card-body .row {\n      margin: 10px 0; }\n      .card-producto .card-body .row input {\n        text-align: center; }\n    .card-producto .card-body .linkAnadir {\n      color: #93072E;\n      text-align: center;\n      font-weight: bold; }\n\n.hoverProd {\n  position: absolute;\n  width: 100%;\n  height: 181px;\n  background-color: rgba(1, 1, 1, 0.38);\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  opacity: 0; }\n  .hoverProd button {\n    position: absolute;\n    bottom: 10px;\n    left: 86px; }\n\n.hoverProd:hover {\n  opacity: 1;\n  transition: opacity .2s ease-out;\n  -moz-transition: opacity .2s ease-out;\n  -webkit-transition: opacity .2s ease-out;\n  -o-transition: opacity .2s ease-out; }\n\n.infoProdCarro {\n  font-weight: bold; }\n  .infoProdCarro span {\n    color: #93072E; }\n  .infoProdCarro .cantProd {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    margin-bottom: 10px; }\n    .infoProdCarro .cantProd span, .infoProdCarro .cantProd input {\n      margin-right: 10px; }\n  .infoProdCarro p {\n    margin-bottom: 5px; }\n  .infoProdCarro p:last-child {\n    margin-bottom: 10px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/ProductoModule/styles/producto.info.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#detProd {\n  list-style: none;\n  padding: 0;\n  margin: 0; }\n  #detProd li {\n    background-color: #ECECEC;\n    padding: 3px;\n    margin-bottom: 10px; }\n    #detProd li div span {\n      color: #93072E;\n      font-weight: bold; }\n\n#compraProd h3 {\n  color: #93072E; }\n\n#compraProd #secCant {\n  background-color: #f7eeda;\n  padding: 5px;\n  margin: 0; }\n  #compraProd #secCant #valProd {\n    color: #93072E;\n    font-weight: bold;\n    font-size: 20px;\n    margin-bottom: 10px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/ProductoModule/templates/lista.producto.component.html":
/***/ (function(module, exports) {

module.exports = "<nav id=\"menuSec\">\r\n  <ul>\r\n    <!--<li><a (click)=\"filtrarXCat('');\">Todos los productos</a></li>-->\r\n    <!--<li><a (click)=\"filtrarXCat('CF');\">Productos Cafe</a></li>-->\r\n    <!--<li><a (click)=\"filtrarXCat('IS');\">Productos Mercadeo</a></li>-->\r\n    <!--<li><a (click)=\"cambiarTipoVista()\">{{ textoVista }}</a></li>-->\r\n\r\n    <li><a (click)=\"filtrarXCat('');\">{{ 'submenu.all'  | translate }}</a></li>\r\n    <li><a (click)=\"filtrarXCat('CF');\">{{ 'submenu.cafe' | translate }}</a></li>\r\n    <li><a (click)=\"filtrarXCat('IS');\">{{ 'submenu.mercadeo' | translate }}</a></li>\r\n    <li><a (click)=\"cambiarTipoVista()\">{{ textoVista | translate }}</a></li>\r\n  </ul>\r\n  <div class=\"input-group\">\r\n    <!--<input type=\"search\" placeholder=\"Buscar\" class=\"form-control\" [ngModel]=\"filtroNombre\" (ngModelChange)=\"filtrarPorNombre($event)\"/>-->\r\n    <input type=\"search\" placeholder=\"{{ 'submenu.buscar'  | translate }}\" class=\"form-control\" [ngModel]=\"filtroNombre\" (ngModelChange)=\"filtrarPorNombre($event)\"/>\r\n\r\n    <div class=\"input-group-addon\">\r\n      <fa name=\"search\"></fa>\r\n    </div>\r\n  </div>\r\n\r\n</nav>\r\n\r\n<div *ngFor=\"let n of notificaciones\">\r\n  <alert type=\"info\" [dismissible]=\"true\" >\r\n    {{n}}\r\n  </alert>\r\n</div>\r\n\r\n<!--<div class=\"spinner\">-->\r\n  <!--<div class=\"cube1\"></div>-->\r\n  <!--<div class=\"cube2\"></div>-->\r\n<!--</div>-->\r\n\r\n<div *ngIf=\"tipoVista === 0; else vistaTabla\">\r\n  <ul id=\"listaProductos\">\r\n    <li *ngFor=\"let prod of prodFiltrados\">\r\n      <producto [producto]=\"prod\" [view]=\"0\" (onSelect)=\"anadirProducto($event)\"></producto>\r\n    </li>\r\n  </ul>\r\n</div>\r\n\r\n<ng-template #vistaTabla>\r\n  <table id=\"table-productos\" class=\"table table-bordered table-hover\">\r\n    <thead>\r\n      <tr>\r\n        <!--<th>Sku</th>-->\r\n        <!--<th>Descripcion</th>-->\r\n        <!--<th>Unidades por multiplo</th>-->\r\n        <!--<th>Cantidad</th>-->\r\n        <!--<th>Cantidad Cajas</th>-->\r\n        <!--<th>Cantidad Tendidos</th>-->\r\n        <!--<th>Cantidad Pallets</th>-->\r\n\r\n        <th>Sku</th>\r\n        <th>{{ 'listpro.description' | translate}}</th>\r\n        <th>{{ 'listpro.unidades' | translate}}</th>\r\n        <th>{{ 'listpro.cantidad' | translate}}</th>\r\n        <th>{{ 'listpro.cantCajas' | translate}}</th>\r\n        <th>{{ 'listpro.cantTendidos' | translate}}</th>\r\n        <th>{{ 'listpro.cantPallets' | translate}}</th>\r\n\r\n        <th></th>\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n      <tr *ngFor=\"let prod of prodFiltrados\">\r\n        <!--<producto [producto]=\"prod\" [view]=\"1\" (onSelect)=\"anadirProducto($event)\"></producto>-->\r\n        <td>{{ prod.sku | noZeros }}</td>\r\n        <td>{{ prod.nombre }}</td>\r\n        <td>{{ prod.multiplo }}</td>\r\n        <td><input class=\"col form-control\" type=\"number\" min=\"{{ prod.multiplo }}\" value=\"{{ prod.multiplo }}\" step=\"{{ prod.multiplo }}\" [(ngModel)]=\"prod.cantidad\"/></td>\r\n        <!--<td>{{ (prod.cantidad / prod.cantCajas) | number:'1.0-2'}}</td>-->\r\n        <!--<td>{{ ((prod.cantidad / prod.cantCajas) / prod.cantTendidos) | number:'1.0-2' }}</td>-->\r\n        <!--<td>{{ ((prod.cantidad / prod.cantCajas) / prod.cantPallets) | number:'1.0-2' }}</td>-->\r\n        <td>{{ (prod.cantCajas * (prod.cantidad / prod.multiplo)) | number:'1.0-2'}}</td>\r\n        <td>{{ (prod.cantTendidos * (prod.cantidad / prod.multiplo)) | number:'1.0-2' }}</td>\r\n        <td>{{ (prod.cantPallets * (prod.cantidad / prod.multiplo)) | number:'1.0-2' }}</td>\r\n        <td><button class=\"btn\" (click)=\"productoSeleccionado(prod)\">{{'prod.anadir' | translate}}</button></td>\r\n      </tr>\r\n    </tbody>\r\n  </table>\r\n</ng-template>\r\n\r\n\r\n<ng-template #confirmarCompra>\r\n  <div class=\"modal-header\">\r\n    <!--<h4>Confirmar</h4>-->\r\n    <h4>{{ \"btn.confirmar\" | translate }}</h4>\r\n  </div>\r\n  <div class=\"modal-body\" *ngIf=\"prodSelect\">\r\n    <p *ngIf=\"errorMsg; else msgConf\">{{ errorMsg }}</p>\r\n\r\n    <ng-template #msgConf>\r\n      <!--<p>Confirmar que desea agregar {{ prodSelect.cantidad }} und de {{prodSelect.nombre}} a su pedido</p>-->\r\n      <p>{{ 'msgConf.msg1' | translate }} {{ prodSelect.cantidad }} {{ 'msgConf.msg2' | translate }} {{prodSelect.nombre}} {{ 'msgConf.msg3' | translate }}</p>\r\n    </ng-template>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <!--<button class=\"btn btn-light\" (click)=\"modalRef.hide()\">Cerrar</button>-->\r\n    <button class=\"btn btn-light\" (click)=\"modalRef.hide()\">{{ 'btn.cerrar' | translate }}</button>\r\n    <button class=\"btn btn-primary\" (click)=\"adicionarAlCarrito()\" *ngIf=\"errorMsg == ''\">{{ 'btn.aceptar' | translate }}</button>\r\n  </div>\r\n</ng-template>\r\n"

/***/ }),

/***/ "../../../../../src/app/ProductoModule/templates/pallet.component.html":
/***/ (function(module, exports) {

module.exports = "<!--<div class=\"text-center infoPallet\">Contenedor de {{ numContenedor | number:'1.0-2' }}\"</div>-->\r\n<!--<div class=\"text-center infoPallet\">{{devolverPorcentaje() | number:'1.0-2'}}% libre</div>-->\r\n<!--<div class=\"text-center infoPallet\">{{ devolvercantPallets() | number:'1.0-2' }} pallets disponibles</div>-->\r\n\r\n\r\n<div class=\"text-center infoPallet\">{{ 'pallet.info1' | translate }}{{ numContenedor | number:'1.0-2' }}\"</div>\r\n<div class=\"text-center infoPallet\">{{ devolverPorcentaje() | number:'1.0-2'}}% {{\"pallet.free\" | translate }}</div>\r\n<div class=\"text-center infoPallet\">{{ devolvercantPallets() | number:'1.0-2' }} {{ \"pallet.info2\" | translate }}</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/ProductoModule/templates/producto.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"view == 0; else vistaDetallada\" class=\"card card-producto\">\r\n  <img class=\"card-img-top\" [src]=\"producto.urlImg\" onerror=\"this.src = 'assets/materiales/logo.jpg'\" />\r\n  <div class=\"hoverProd\">\r\n    <button class=\"btn\" [routerLink]=\"['/producto', producto.id]\">{{ 'btn.detalles' | translate }}</button>\r\n  </div>\r\n  <div class=\"card-body\">\r\n    <h5>{{ producto.nombre }}</h5>\r\n    <p><span>SKU:</span> {{ producto.sku | noZeros }} </p>\r\n    <p><span>{{'prod.valor'| translate }}:</span> {{ producto.valor }} </p>\r\n    <div class=\"row\">\r\n      <input class=\"col form-control\" type=\"number\" min=\"{{ producto.multiplo }}\" value=\"{{ producto.multiplo }}\" step=\"{{ producto.multiplo }}\" [(ngModel)]=\"producto.cantidad\"/>\r\n      <a class=\"col linkAnadir\" (click)=\"productoSeleccionado()\">{{'prod.anadir' | translate}}</a>\r\n    </div>\r\n    <p>{{'prod.detalle' | translate}} {{ producto.multiplo }}. {{'prod.eje' | translate}} {{ producto.multiplo * 2 }}, {{producto.multiplo * 3}}</p>\r\n  </div>\r\n</div>\r\n\r\n<ng-template #vistaDetallada >\r\n    <!--<td>{{ producto.sku }}</td>-->\r\n    <!--<td>{{ producto.nombre }}</td>-->\r\n    <!--<td>{{ producto.multiplo }}</td>-->\r\n    <!--<td><input class=\"col form-control\" type=\"number\" min=\"{{ producto.multiplo }}\" value=\"{{ producto.multiplo }}\" step=\"{{ producto.multiplo }}\" [(ngModel)]=\"producto.cantidad\"/></td>-->\r\n    <!--<td>cant cajas</td>-->\r\n    <!--<td>cant tendidos</td>-->\r\n    <!--<td>cant pallets</td>-->\r\n    <!--<td><button class=\"btn btn-light\" (click)=\"productoSeleccionado()\">Anadir Producto</button></td>-->\r\n\r\n  <div class=\"col-sm-2\">\r\n    <img class=\"img-fluid\" [src]=\"producto.urlImg\" onerror=\"this.src = 'assets/materiales/logo.jpg'\" >\r\n  </div>\r\n  <div class=\"col-sm-5 infoProdCarro\">\r\n    <h5 class=\"text-redJV\" style=\"margin-bottom: 15px;\">{{ producto.nombre }}</h5>\r\n    <p>\r\n      <span>Sku:</span> {{ producto.sku | noZeros }}\r\n    </p>\r\n    <p>\r\n      <span>{{'prod.valor' | translate}}:</span> {{ producto.valor }}\r\n    </p>\r\n    <div>\r\n      <p>{{'prod.detalle' | translate}} {{ producto.multiplo }}. {{'prod.eje' | translate}} {{ producto.multiplo * 2 }}, {{producto.multiplo * 3}}</p>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-5 infoProdCarro\">\r\n    <div class=\"cantProd\">\r\n      <span>{{'listpro.cantidad' | translate}}:</span>\r\n      <input class=\"col form-control\" type=\"number\" min=\"{{ producto.multiplo }}\" value=\"{{ producto.cantidad }}\" step=\"{{ producto.multiplo }}\" [(ngModel)]=\"producto.cantidad\"/>\r\n      <button class=\"btn\" (click)=\"actualizarProducto()\">{{ 'btn.actualizar' | translate }}</button>\r\n    </div>\r\n    <div>\r\n      <p><span>{{ 'car.cajas' | translate }}:</span> {{ (producto.cantCajas * (producto.cantidad / producto.multiplo)) | number:'1.0-2' }}</p>\r\n      <p><span>{{ 'car.tendidos' | translate }}:</span> {{(producto.cantTendidos * (producto.cantidad / producto.multiplo)) | number:'1.0-2'}}</p>\r\n      <p><span>{{ 'car.pallets' | translate }}:</span> {{ (producto.cantPallets * (producto.cantidad / producto.multiplo)) | number:'1.0-2'}}</p>\r\n      <p><span>{{ 'prod.total' | translate }}:</span> {{ producto.cantidad * producto.valor | number:'1.0-2'}}</p>\r\n    </div>\r\n    <div>\r\n      <button (click)=\"borrarProducto()\" class=\"btn\">{{ 'btn.quitarPro' | translate }}</button>\r\n    </div>\r\n  </div>\r\n</ng-template>\r\n"

/***/ }),

/***/ "../../../../../src/app/ProductoModule/templates/producto.info.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <a [routerLink]=\"['/']\" class=\"btn btn-link text-redJV\">Regresar</a>\r\n</div>\r\n\r\n<div class=\"row\" *ngIf=\"producto\">\r\n  <div class=\"col-sm-3\">\r\n    <!--<h5>Informacion Adicional</h5>-->\r\n    <h5>{{'prod.infoTitle' | translate }}</h5>\r\n    <ul id=\"detProd\">\r\n      <li>\r\n        <div><span>Sku:</span></div>\r\n        <div>{{ producto.sku | noZeros }}</div>\r\n      </li>\r\n      <li>\r\n        <!--<div><span>Peso Neto:</span></div>-->\r\n        <div><span>{{'prod.pesoNeto' | translate }}</span></div>\r\n        <div>{{ producto.pesoNeto }}</div>\r\n      </li>\r\n      <li>\r\n        <!--<div><span>Cantidad por caja</span></div>-->\r\n        <div><span>{{'prod.cantCaja' | translate}}</span></div>\r\n        <div>{{ producto.cantXCaja }}</div>\r\n      </li>\r\n      <li>\r\n        <!--<div><span>Peso bruto de la caja</span></div>-->\r\n        <div><span>{{'prod.pesoBrutoCaja' | translate}}</span></div>\r\n        <div>{{ producto.pesoBrutoxCaja }}</div>\r\n      </li>\r\n      <li>\r\n        <!--<div><span>Total cajas por pallet</span></div>-->\r\n        <div><span>{{'prod.totalCajasPallet' | translate}}</span></div>\r\n        <div>{{ producto.totalCajasXPallet }}</div>\r\n      </li>\r\n      <li>\r\n        <!--<div><span>Vida Util</span></div>-->\r\n        <div><span>{{'prod.vidaUtil' | translate}}</span></div>\r\n        <div>{{ producto.vidaUtil }}</div>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n  <div class=\"col-sm-5\">\r\n    <img [src]=\"producto.urlImg\" onerror=\"this.src = 'assets/materiales/logo.jpg'\" class=\"img-fluid\" />\r\n  </div>\r\n  <div class=\"col-sm-4\" id=\"compraProd\">\r\n    <h3>{{ producto.nombre }}</h3>\r\n    <div class=\"row\" id=\"secCant\">\r\n      <div class=\"col\">\r\n        <div>{{'listpro.cantidad' | translate}}</div>\r\n        <input class=\"form-control\" type=\"number\" min=\"{{ producto.multiplo }}\" value=\"{{ producto.multiplo }}\" step=\"{{ producto.multiplo }}\" [(ngModel)]=\"producto.cantidad\"/>\r\n      </div>\r\n      <div class=\"col\">\r\n        <div>{{'prod.valor' | translate}}</div>\r\n        <div id=\"valProd\">{{ producto.cantidad * producto.valor }} USD</div>\r\n        <button class=\"btn btn-primary\" (click)=\"anadirProducto()\">{{'btn.anadirPedido' | translate}}</button>\r\n      </div>\r\n    </div>\r\n    <p>{{'prod.detalle' | translate }} {{ producto.multiplo }}. {{'prod.eje' | translate}} {{ producto.multiplo * 2 }}, {{producto.multiplo * 3}}</p>\r\n  </div>\r\n</div>\r\n\r\n<ng-template #confirmarCompra>\r\n  <div class=\"modal-header\">\r\n    <h4>{{ \"btn.confirmar\" | translate }}</h4>\r\n  </div>\r\n  <div class=\"modal-body\">\r\n    <p *ngIf=\"errorMsg; else msgConf\">{{ errorMsg }}</p>\r\n\r\n    <ng-template #msgConf>\r\n      <p>{{ 'msgConf.msg1' | translate }} {{ producto.cantidad }} {{ 'msgConf.msg2' | translate }} {{producto.nombre}} {{ 'msgConf.msg3' | translate }}</p>\r\n    </ng-template>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <button class=\"btn btn-light\" (click)=\"modalRef.hide()\">{{ 'btn.cerrar' | translate }}</button>\r\n    <button class=\"btn btn-primary\" (click)=\"adicionarAlCarrito()\" *ngIf=\"errorMsg == ''\">{{ 'btn.aceptar' | translate }}</button>\r\n  </div>\r\n</ng-template>\r\n"

/***/ }),

/***/ "../../../../../src/app/UsuarioModule/components/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_usuario_services__ = __webpack_require__("../../../../../src/app/UsuarioModule/services/usuario.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_jwt__ = __webpack_require__("../../../../angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_jwt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_translate__ = __webpack_require__("../../../../ng2-translate/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = (function () {
    function LoginComponent(router, us, translate) {
        this.router = router;
        this.us = us;
        this.translate = translate;
        this.error = "";
        this.user = {
            username: "",
            password: ""
        };
    }
    LoginComponent.prototype.iniciarSesion = function () {
        var _this = this;
        console.log(this.user);
        var jwtHelper = new __WEBPACK_IMPORTED_MODULE_3_angular2_jwt__["JwtHelper"]();
        this.us.loguearUsuario(this.user)
            .subscribe(function (res) {
            console.log(res);
            if (res.codigo == 0) {
                var dataToken = jwtHelper.decodeToken(res.datos);
                localStorage.setItem("cliente", dataToken.cliente);
                localStorage.setItem('destinatario', dataToken.destinatario);
                localStorage.setItem("canal", dataToken.canal);
                localStorage.setItem('orgVenta', dataToken.orgVenta);
                localStorage.setItem('almacen', dataToken.almacen);
                localStorage.setItem('centro', dataToken.centro);
                localStorage.setItem("token", res.datos);
                _this.router.navigate(['/']);
                console.log(res);
            }
            else {
                //this.error = res.datos.message;
                console.log(res);
            }
        }, function (error) {
            console.log(error);
        });
        // this.authHttp.post('sdfsdfsdfsdf', this.user)
        //   .subscribe(data => {
        //
        //   },
        //     error => {
        //
        //     });
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "login",
        template: __webpack_require__("../../../../../src/app/UsuarioModule/templates/login.component.html"),
        styles: [__webpack_require__("../../../../../src/app/UsuarioModule/styles/login.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_usuario_services__["a" /* UsuarioServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_usuario_services__["a" /* UsuarioServices */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_translate__["b" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ng2_translate__["b" /* TranslateService */]) === "function" && _c || Object])
], LoginComponent);

var _a, _b, _c;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ "../../../../../src/app/UsuarioModule/components/logout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogoutComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var LogoutComponent = (function () {
    function LogoutComponent() {
    }
    return LogoutComponent;
}());
LogoutComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "logout",
        template: "<p></p>"
    })
], LogoutComponent);

//# sourceMappingURL=logout.component.js.map

/***/ }),

/***/ "../../../../../src/app/UsuarioModule/model/usuario.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Usuario; });
var Usuario = (function () {
    function Usuario() {
        this.id = 0;
        this.id_usuario = 0;
        this.id_cliente = 0;
        this.id_canal = 0;
        this.username = "";
        this.password = "";
        this.salt = "";
        this.cambiopassword = true;
        this.esComercial = false;
        this.token = "";
        this.rol = [];
        this.directorio = [];
    }
    return Usuario;
}());

//# sourceMappingURL=usuario.model.js.map

/***/ }),

/***/ "../../../../../src/app/UsuarioModule/services/usuario.services.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__GeneralModule_services_globals__ = __webpack_require__("../../../../../src/app/GeneralModule/services/globals.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_jwt__ = __webpack_require__("../../../../angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angular2_jwt__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuarioServices; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UsuarioServices = (function () {
    function UsuarioServices(globals, http, router, authHttp) {
        this.globals = globals;
        this.http = http;
        this.router = router;
        this.authHttp = authHttp;
    }
    UsuarioServices.prototype.loguearUsuario = function (user) {
        return this.http.post(this.globals.urlServer + "/login", user).map(function (res) { return res.json(); });
    };
    UsuarioServices.prototype.getAllUser = function () {
        return this.authHttp.get(this.globals.urlServer + "/usuario/all").map(function (res) { return res.json(); });
    };
    UsuarioServices.prototype.getUser = function (id) {
        return this.authHttp.get(this.globals.urlServer + "/usuario/view", {
            search: { id: id }
        }).map(function (res) { return res.json(); });
    };
    UsuarioServices.prototype.saveUser = function (user, newUser) {
        return this.authHttp.post(this.globals.urlServer + "/usuario/save", { user: user, isNew: newUser }).map(function (res) { return res.json(); });
    };
    UsuarioServices.prototype.getAllClientesSAP = function () {
        return this.authHttp.get(this.globals.urlServer + "/usuario/clientes").map(function (res) { return res.json(); });
    };
    UsuarioServices.prototype.deleteUser = function (id) {
        return this.authHttp.post(this.globals.urlServer + "/usuario/delete", { id: id }).map(function (res) { return res.json(); });
    };
    UsuarioServices.prototype.isLogin = function () {
        if (localStorage.getItem('token') == null) {
            return false;
        }
        else {
            return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_4_angular2_jwt__["tokenNotExpired"])();
        }
    };
    UsuarioServices.prototype.logout = function () {
        localStorage.removeItem("token");
        localStorage.removeItem("cliente");
        localStorage.removeItem("canal");
        this.irALogin();
    };
    UsuarioServices.prototype.irALogin = function () {
        this.router.navigate(['/login']);
    };
    return UsuarioServices;
}());
UsuarioServices = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__GeneralModule_services_globals__["a" /* GlobalServices */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__GeneralModule_services_globals__["a" /* GlobalServices */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4_angular2_jwt__["AuthHttp"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_angular2_jwt__["AuthHttp"]) === "function" && _d || Object])
], UsuarioServices);

var _a, _b, _c, _d;
//# sourceMappingURL=usuario.services.js.map

/***/ }),

/***/ "../../../../../src/app/UsuarioModule/styles/login.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#contLogin {\n  padding: 20px;\n  font-family: Tahoma, Geneva, san-serif; }\n  #contLogin h3 {\n    color: #b10135;\n    font-size: 30px; }\n  #contLogin #formLogin {\n    background: url(\"/assets/img/fondo_login.jpg\") no-repeat;\n    height: 400px;\n    padding: 30px;\n    background-size: 100% 100%; }\n    #contLogin #formLogin div {\n      margin: auto; }\n    #contLogin #formLogin a.btn-danger {\n      color: #ffffff; }\n    #contLogin #formLogin input {\n      margin-right: 15px; }\n  #contLogin p {\n    color: #717171;\n    font-size: 21px;\n    font-weight: normal;\n    margin-bottom: 40px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/UsuarioModule/templates/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"contLogin\">\r\n  <form id=\"formLogin\" class=\"form-inline\">\r\n    <div>\r\n      <h3 >{{ 'site.welcome1' | translate }}<span style=\"font-size: 12px\">®</span> {{ 'site.welcome2' | translate }} <span style=\"color:#CC9966\">{{ 'site.welcome3' | translate }}</span></h3>\r\n      <!--<h3 >Bienvenidos a Juan Valdez<span style=\"font-size: 12px\">®</span> Café <span style=\"color:#CC9966\">Internacional</span></h3>-->\r\n      <p>{{'site.encabezado1' | translate}}</p>\r\n      <!--<p>Si ya eres miembro, ingresa tus datos para empezar</p>-->\r\n      <div class=\"form-group\">\r\n        <input id=\"username\" name=\"username\" type=\"text\" #username=\"ngModel\" [(ngModel)]=\"user.username\" placeholder=\"{{ 'site.placeholder1' | translate}}\" required class=\"form-control\" />\r\n        <input id=\"password\" name=\"password\" type=\"password\" #password=\"ngModel\" [(ngModel)]=\"user.password\" placeholder=\"{{ 'site.placeholder2' |  translate}}\" required class=\"form-control\" />\r\n        <a class=\"btn btn-danger\" (click)=\"iniciarSesion()\">{{ 'site.btnLogin' | translate }}</a>\r\n        <!--<a class=\"btn btn-danger\" (click)=\"iniciarSesion()\">Iniciar Sesion</a>-->\r\n      </div>\r\n     <div class=\"form-group\" *ngIf=\"error\" style=\"margin-top: 20px;\">\r\n       <alert type=\"warning\">\r\n         {{ error }}\r\n       </alert>\r\n     </div>\r\n    </div>\r\n  </form>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/UsuarioModule/usuario.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_login_component__ = __webpack_require__("../../../../../src/app/UsuarioModule/components/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_logout_component__ = __webpack_require__("../../../../../src/app/UsuarioModule/components/logout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__usuario_routing_module__ = __webpack_require__("../../../../../src/app/UsuarioModule/usuario.routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_translate__ = __webpack_require__("../../../../ng2-translate/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuarioModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var UsuarioModule = (function () {
    function UsuarioModule() {
    }
    return UsuarioModule;
}());
UsuarioModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["k" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_4__usuario_routing_module__["a" /* UsuarioRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__["b" /* AlertModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_7_ng2_translate__["a" /* TranslateModule */],
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__components_login_component__["a" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_3__components_logout_component__["a" /* LogoutComponent */]
        ],
    })
], UsuarioModule);

//# sourceMappingURL=usuario.module.js.map

/***/ }),

/***/ "../../../../../src/app/UsuarioModule/usuario.routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_login_component__ = __webpack_require__("../../../../../src/app/UsuarioModule/components/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_logout_component__ = __webpack_require__("../../../../../src/app/UsuarioModule/components/logout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_usuario_services__ = __webpack_require__("../../../../../src/app/UsuarioModule/services/usuario.services.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuarioRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var userRoutes = [
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_1__components_login_component__["a" /* LoginComponent */] },
    { path: 'logout', component: __WEBPACK_IMPORTED_MODULE_2__components_logout_component__["a" /* LogoutComponent */] }
];
var UsuarioRoutingModule = (function () {
    function UsuarioRoutingModule() {
    }
    return UsuarioRoutingModule;
}());
UsuarioRoutingModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */].forChild(userRoutes)
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_4__services_usuario_services__["a" /* UsuarioServices */]],
        exports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */]
        ]
    })
], UsuarioRoutingModule);

//# sourceMappingURL=usuario.routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<!--<div style=\"text-align:center\">-->\n  <!--<h1>-->\n    <!--Welcome to {{title}}!-->\n  <!--</h1>-->\n  <!--<img width=\"300\" src=\"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxOS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAyNTAgMjUwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAyNTAgMjUwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7ZmlsbDojREQwMDMxO30NCgkuc3Qxe2ZpbGw6I0MzMDAyRjt9DQoJLnN0MntmaWxsOiNGRkZGRkY7fQ0KPC9zdHlsZT4NCjxnPg0KCTxwb2x5Z29uIGNsYXNzPSJzdDAiIHBvaW50cz0iMTI1LDMwIDEyNSwzMCAxMjUsMzAgMzEuOSw2My4yIDQ2LjEsMTg2LjMgMTI1LDIzMCAxMjUsMjMwIDEyNSwyMzAgMjAzLjksMTg2LjMgMjE4LjEsNjMuMiAJIi8+DQoJPHBvbHlnb24gY2xhc3M9InN0MSIgcG9pbnRzPSIxMjUsMzAgMTI1LDUyLjIgMTI1LDUyLjEgMTI1LDE1My40IDEyNSwxNTMuNCAxMjUsMjMwIDEyNSwyMzAgMjAzLjksMTg2LjMgMjE4LjEsNjMuMiAxMjUsMzAgCSIvPg0KCTxwYXRoIGNsYXNzPSJzdDIiIGQ9Ik0xMjUsNTIuMUw2Ni44LDE4Mi42aDBoMjEuN2gwbDExLjctMjkuMmg0OS40bDExLjcsMjkuMmgwaDIxLjdoMEwxMjUsNTIuMUwxMjUsNTIuMUwxMjUsNTIuMUwxMjUsNTIuMQ0KCQlMMTI1LDUyLjF6IE0xNDIsMTM1LjRIMTA4bDE3LTQwLjlMMTQyLDEzNS40eiIvPg0KPC9nPg0KPC9zdmc+DQo=\">-->\n<!--</div>-->\n<!--<h2>Here are some links to help you start: </h2>-->\n<!--<ul>-->\n  <!--<li>-->\n    <!--<h2><a target=\"_blank\" href=\"https://angular.io/tutorial\">Tour of Heroes</a></h2>-->\n  <!--</li>-->\n  <!--<li>-->\n    <!--<h2><a target=\"_blank\" href=\"https://github.com/angular/angular-cli/wiki\">CLI Documentation</a></h2>-->\n  <!--</li>-->\n  <!--<li>-->\n    <!--<h2><a target=\"_blank\" href=\"http://angularjs.blogspot.com/\">Angular blog</a></h2>-->\n  <!--</li>-->\n<!--</ul>-->\n<div class=\"container\">\n  <header-app></header-app>\n  <div id=\"mainApp\">\n    <router-outlet></router-outlet>\n  </div>\n  <footer-app></footer-app>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#mainApp {\n  padding: 0 10px 10px 10px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_translate__ = __webpack_require__("../../../../ng2-translate/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = AppComponent_1 = (function () {
    function AppComponent(translate) {
        this.translate = translate;
        this.title = 'app';
        // idiomas disponibles
        translate.addLangs(["en", "es"]);
        //idioma por defecto
        translate.setDefaultLang(AppComponent_1.idiomaAct);
        // idioma que vamos a usar
        translate.use(AppComponent_1.idiomaAct);
    }
    return AppComponent;
}());
AppComponent.idiomaAct = "es";
AppComponent = AppComponent_1 = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_translate__["b" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_translate__["b" /* TranslateService */]) === "function" && _a || Object])
], AppComponent);

var AppComponent_1, _a;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__GeneralModule_general_module__ = __webpack_require__("../../../../../src/app/GeneralModule/general.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__GeneralModule_services_globals__ = __webpack_require__("../../../../../src/app/GeneralModule/services/globals.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_routing_module__ = __webpack_require__("../../../../../src/app/app.routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__UsuarioModule_usuario_module__ = __webpack_require__("../../../../../src/app/UsuarioModule/usuario.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ProductoModule_producto_module__ = __webpack_require__("../../../../../src/app/ProductoModule/producto.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__GeneralModule_services_carro_services__ = __webpack_require__("../../../../../src/app/GeneralModule/services/carro.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ProductoModule_services_producto_services__ = __webpack_require__("../../../../../src/app/ProductoModule/services/producto.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__AuthModule_auth_module__ = __webpack_require__("../../../../../src/app/AuthModule/auth.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__AuthModule_services_auth_guard_services__ = __webpack_require__("../../../../../src/app/AuthModule/services/auth-guard.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__AdminModule_admin_module__ = __webpack_require__("../../../../../src/app/AdminModule/admin.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_ng2_translate__ = __webpack_require__("../../../../ng2-translate/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_angular_font_awesome__ = __webpack_require__("../../../../angular-font-awesome/dist/angular-font-awesome.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_5__app_routing_module__["a" /* AppRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_3__GeneralModule_general_module__["a" /* GeneralModule */],
            __WEBPACK_IMPORTED_MODULE_6__UsuarioModule_usuario_module__["a" /* UsuarioModule */],
            __WEBPACK_IMPORTED_MODULE_8__ProductoModule_producto_module__["a" /* ProductoModule */],
            __WEBPACK_IMPORTED_MODULE_11__AuthModule_auth_module__["a" /* AuthModule */],
            __WEBPACK_IMPORTED_MODULE_13__AdminModule_admin_module__["a" /* AdminModule */],
            __WEBPACK_IMPORTED_MODULE_14_ng2_translate__["a" /* TranslateModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_15_angular_font_awesome__["a" /* AngularFontAwesomeModule */]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_4__GeneralModule_services_globals__["a" /* GlobalServices */], __WEBPACK_IMPORTED_MODULE_9__GeneralModule_services_carro_services__["a" /* CarroServices */], __WEBPACK_IMPORTED_MODULE_10__ProductoModule_services_producto_services__["a" /* ProductoServices */], __WEBPACK_IMPORTED_MODULE_12__AuthModule_services_auth_guard_services__["a" /* AuthGuardServices */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var appRoutes = [];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */].forRoot(appRoutes)
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */]
        ]
    })
], AppRoutingModule);

//# sourceMappingURL=app.routing.module.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: true
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[1]);
//# sourceMappingURL=main.bundle.js.map